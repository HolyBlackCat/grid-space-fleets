#ifndef LOGIC_H_INCLUDED
#define LOGIC_H_INCLUDED

#include "lib_sdl.h"
#include "lib_sdlnet.h"
#include "lib_gl.h"
#include "lib_al.h"
#include "lib_zlib.h"
#include "lib_ogg.h"
#include "lib_vorbis.h"
#include "audio.h"
#include "exceptions.h"
#include "graphics.h"
#include "input.h"
#include "math.h"
#include "network.h"
#include "system.h"
#include "utils.h"
#include "window.h"

namespace Layouts
{
    using Flat = Graphics::TypePack<fvec2, fvec2, fvec4, fvec2>;
    using GUI = Graphics::TypePack<fvec2>;

    using World = Graphics::TypePack<fvec3, fvec3, fvec2, fvec4, fvec2, float>;
}


#endif