#ifndef GRAPHICS_H_INCLUDED
#define GRAPHICS_H_INCLUDED

#include <cstddef>
#include <cstring>
#include <initializer_list>
#include <typeinfo>
#include <typeindex>
#include <utility>

#include "exceptions.h"
#include "lib_gl.h"
#include "math.h"
#include "os.h"
#include "system.h"
#include "utils.h"
#include "window.h"

/* GLSL version chart:
    1.10.59		2.0		April 2004		#version 110
    1.20.8		2.1		September 2006	#version 120
    1.30.10		3.0		August 2008		#version 130
    1.40.08		3.1		March 2009		#version 140
    1.50.11		3.2		August 2009		#version 150
    3.30.6		3.3		February 2010	#version 330
    4.00.9		4.0		March 2010		#version 400
    4.10.6		4.1		July 2010		#version 410
    4.20.11		4.2		August 2011		#version 420
    4.30.8		4.3		August 2012		#version 430
    4.40		4.4		July 2013		#version 440
    4.50		4.5		August 2014		#version 450
    1.00        ES 2                    #version 100

    GLSL ES can be tested with `#ifdef GL_ES`.
    GLSL ES lacks default precision for float inside of fragment shaders.
*/

namespace Graphics
{
    #ifdef LXINTERNAL_GRAPHICS_H_SPECIAL_ACCESS
    void Init();
    void BeginFrame();
    void EndFrame();
    #endif

    // Here and below 01 means that colors are in [0;1] range. Otherwise it's [0;255].

    namespace Blend
    {
        enum Factors
        {
            zero                 = GL_ZERO,
            one                  = GL_ONE,
            src                  = GL_SRC_COLOR,
            one_minus_src        = GL_ONE_MINUS_SRC_COLOR,
            dst                  = GL_DST_COLOR,
            one_minus_dst        = GL_ONE_MINUS_DST_COLOR,
            src_a                = GL_SRC_ALPHA,
            one_minus_src_a      = GL_ONE_MINUS_SRC_ALPHA,
            dst_a                = GL_DST_ALPHA,
            one_minus_dst_a      = GL_ONE_MINUS_DST_ALPHA,
            constant             = GL_CONSTANT_COLOR,
            one_minus_constant   = GL_ONE_MINUS_CONSTANT_COLOR,
            constant_a           = GL_CONSTANT_ALPHA,
            one_minus_constant_a = GL_ONE_MINUS_CONSTANT_ALPHA,
            src_a_saturate       = GL_SRC_ALPHA_SATURATE,
            ForPC
            (
                src1                 = GL_SRC1_COLOR,
                one_minus_src1       = GL_ONE_MINUS_SRC1_COLOR,
                src1_a               = GL_SRC1_ALPHA,
                one_minus_src1_a     = GL_ONE_MINUS_SRC1_ALPHA,
            )
        };
        enum Equations
        {
            eq_add              = GL_FUNC_ADD,
            eq_subtract         = GL_FUNC_SUBTRACT,
            eq_reverse_subtract = GL_FUNC_REVERSE_SUBTRACT,
            ForPC
            (
                eq_min              = GL_MIN,
                eq_max              = GL_MAX,
            )
        };

        // Func(a,b) and Equation(a) set same parameters for both color and alpha blending.
        // Func(a,b,c,d) and Equation(a,b) set same parameters for color and alpha blending separately.
        inline void Enable() {glEnable(GL_BLEND);}
        inline void Disable() {glDisable(GL_BLEND);}
        inline void Func(Factors src, Factors dst)                             {glBlendFunc(src, dst);}
        inline void Func(Factors src, Factors dst, Factors srca, Factors dsta) {glBlendFuncSeparate(src, dst, srca, dsta);}
        inline void Equation(Equations eq) {glBlendEquation(eq);}
        inline void Equation(Equations eq, Equations eqa) {glBlendEquationSeparate(eq, eqa);}

        namespace Presets
        {
            inline void FuncOverwrite    () {Func(one, zero);}
            inline void FuncSimple       () {Func(src_a, one_minus_src_a);} // This one is crappy.
            inline void FuncNormal_RawToPre() {Func(src_a, one_minus_src_a, one, one_minus_src_a);} // Src textures are normal, output is premultiplied.
            inline void FuncNormal_Pre     () {Func(one, one_minus_src_a);} // Src and output are premultiplited
        }
    }

    void Depth(bool on);
    inline void Culling(bool on) {(on ? glEnable : glDisable)(GL_CULL_FACE);}
    inline void ClearColor(u8vec4 c) {glClearColor(c.r / 255.0f, c.g / 255.0f, c.b / 255.0f, c.a / 255.0f);}
    inline void ClearColor01(fvec4 c) {glClearColor(c.r, c.g, c.b, c.a);}

    // These functions set a size and a position or a rectange to draw into. You need to call one of them before rendering to thing with a different size or when the window is resized.
    // __2 ones can increase viewport size by one extra pixel in each dimension to ensure that size % 2 == 0. They are necessary for good 2D pixel graphics.
    // __2y are same, but if they increase Y size, they also decrease viewport Y position by 1. These are good for the window.
    inline void Viewport(ivec2 pos, ivec2 size) {glViewport(pos.x, pos.y, size.x, size.y);}
    inline void Viewport(ivec2 size)            {Viewport({0, 0}, {size.x, size.y});}
    inline void Viewport2(ivec2 size)           {Viewport({0, 0}, size+size%2);}
    inline void Viewport2y(ivec2 size)          {Viewport({0, -size.x%2}, size+size%2);}
    template <typename T>
    void ViewportObj(const T &obj)     {Viewport((ivec2)obj.Size());}
    template <typename T>
    void ViewportObj2(const T &obj)    {Viewport2((ivec2)obj.Size());}
    template <typename T>
    void ViewportObj2y(const T &obj)   {Viewport2y((ivec2)obj.Size());}
    inline void ViewportFullscreen()   {Viewport(Window::Size());}
    inline void ViewportFullscreen2()  {Viewport2(Window::Size());}
    inline void ViewportFullscreen2y() {Viewport2y(Window::Size());}


    void ForceErrorCheck();

    inline namespace Templates
    {
        template <typename T> struct type2glconst;
        template <> struct type2glconst<unsigned char>  {static constexpr GLenum value = GL_UNSIGNED_BYTE;};
        template <> struct type2glconst<signed char>    {static constexpr GLenum value = GL_BYTE;};
        template <> struct type2glconst<char>           {static constexpr GLenum value = Utils::SysInfo::char_signed ? GL_BYTE : GL_UNSIGNED_BYTE;};
        template <> struct type2glconst<unsigned short> {static constexpr GLenum value = GL_UNSIGNED_SHORT;};
        template <> struct type2glconst<signed short>   {static constexpr GLenum value = GL_SHORT;};
        template <> struct type2glconst<unsigned int>   {static constexpr GLenum value = GL_UNSIGNED_INT;};
        template <> struct type2glconst<signed int>     {static constexpr GLenum value = GL_INT;};
        template <> struct type2glconst<float>          {static constexpr GLenum value = GL_FLOAT;};
        #ifdef GL_DOUBLE
        template <> struct type2glconst<double>         {static constexpr GLenum value = GL_DOUBLE;};
        #endif

        template <typename T>
        inline void SetAttribPointer(GLuint index, GLint size, GLsizei stride, const GLvoid *pointer)
        {
            static_assert(type2glconst<T>::value, "Invalid type!");
            #ifdef glVertexAttribIPointer
            glVertexAttribIPointer(index, size, type2glconst<T>::value, stride, pointer);
            #endif
        }
        template <>
        inline void SetAttribPointer<float>(GLuint index, GLint size, GLsizei stride, const GLvoid *pointer)
        {
            glVertexAttribPointer(index, size, type2glconst<float>::value, GL_FALSE, stride, pointer);
        }
        template <>
        inline void SetAttribPointer<double>(GLuint index, GLint size, GLsizei stride, const GLvoid *pointer)
        {
            #ifdef glVertexAttribLPointer
            glVertexAttribLPointer(index, size, type2glconst<double>::value, stride, pointer);
            #endif
        }
    }

    inline namespace TypePackDefinition
    {
        template <typename F, typename ...P> struct TypePack;

        namespace InternalPackTemplates
        {
            template <unsigned int N, typename F, typename ...P> struct At
            {
                static auto &func(TypePack<F, P...> *th) {return At<N-1, P...>::func(&th->next);}
            };
            template <typename F, typename ...P> struct At<0, F, P...>
            {
                static auto &func(TypePack<F, P...> *th) {return th->first;}
            };
        }

        template <typename F, typename ...P> struct TypePack
        {
            static constexpr unsigned int size = sizeof...(P)+1;
            F first;
            TypePack<P...> next;
            TypePack() {}
            TypePack(const F &f, const P &... p) : first(f), next(p...) {}
            template <unsigned int N> using TypeAt = typename Utils::TypeAt<N, F, P...>::type;
            template <unsigned int N> auto &At() {return InternalPackTemplates::At<N, F, P...>::func(this);}
        };
        template <typename F> struct TypePack<F>
        {
            static constexpr unsigned int size = 1;
            F first;
            TypePack() {}
            TypePack(const F &f) : first(f) {}
            template <unsigned int N> using TypeAt = typename Utils::TypeAt<N, F>::type;
            template <unsigned int N> auto &At() {return InternalPackTemplates::At<N, F>::func(this);}
        };
    }


    namespace Attribs
    {
        uint64_t Status();
        void SetStatus(uint64_t new_status);
        void Enable(unsigned int num);
        void Disable(unsigned int num);

        inline namespace PackSetup
        {
            namespace Internal
            {
                template <typename T> struct DimOf {static constexpr unsigned int value = 1;};
                template <unsigned int D, typename T> struct DimOf<vec<D, T>> {static constexpr unsigned int value = D;};
                template <typename T> struct BaseTypeOf {using type = T;};
                template <unsigned int D, typename T> struct BaseTypeOf<vec<D, T>> {using type = T;};


                template <unsigned int SIZE, unsigned int POS, unsigned int BYTEPOS, typename T> struct SetAttribs
                {
                    static void func() {}
                };
                template <unsigned int SIZE, unsigned int POS, unsigned int BYTEPOS, typename F, typename ...P> struct SetAttribs<SIZE, POS, BYTEPOS, TypePack<F, P...>>
                {
                    static void func()
                    {
                        SetAttribPointer<typename BaseTypeOf<typename TypePack<F, P...>::template TypeAt<0>>::type>(POS, DimOf<F>::value, SIZE, (void *)BYTEPOS);
                        using pack = TypePack<F, P...>;
                        SetAttribs<SIZE, POS+1, BYTEPOS + offsetof(pack, next), TypePack<P...>>::func();
                    }
                };
                template <unsigned int SIZE, unsigned int POS, unsigned int BYTEPOS, typename F> struct SetAttribs<SIZE, POS, BYTEPOS, TypePack<F>>
                {
                    static void func()
                    {
                        SetAttribPointer<typename BaseTypeOf<typename TypePack<F>::template TypeAt<0>>::type>(POS, DimOf<F>::value, SIZE, (void *)BYTEPOS);
                    }
                };
            }

            template <typename T> void SetForType()
            {
                Attribs::SetStatus((1ull << T::size) - 1);
                Internal::SetAttribs<sizeof (T), 0, 0, T>::func();
            }
        }
    }


    enum class StorageType : GLenum
    {
        draw_static  = GL_STATIC_DRAW,
        draw_dynamic = GL_DYNAMIC_DRAW,
        draw_stream  = GL_STREAM_DRAW,
        #ifdef GL_STATIC_COPY
        copy_static  = GL_STATIC_COPY,
        copy_dynamic = GL_DYNAMIC_COPY,
        copy_stream  = GL_STREAM_COPY,
        read_static  = GL_STATIC_READ,
        read_dynamic = GL_DYNAMIC_READ,
        read_stream  = GL_STREAM_READ,
        #endif
    };

    class InternalState
    {
        template <typename T> friend class VertexArray;
        friend class Shader;
        static GLuint vbo_binding;
        static bool vbo_attribs_configured;

        static GLuint shader_binding;
    };

    template <typename T>
    class VertexArray
    {
        static_assert(sizeof (T), "Void is not allowed as a parameter.");
        GLuint vbo;
      public:
        using Element = T;

        VertexArray()
        {
            static unsigned int id = 0;
            glGenBuffers(1, &vbo);
            if (!vbo)
                Sys::Error(Jo("glGenBuffers() failed for VertexArray #", id, '!'));
            id++;
        }
        VertexArray(Utils::ArrayViewer<T> src, StorageType acc = StorageType::draw_static) : VertexArray() // (`data` may be null) Binds VBO after construction.
        {
            NewData(src, acc);
        }
        VertexArray(Utils::ArrayViewer<uint8_t> src, StorageType acc = StorageType::draw_static) : VertexArray() // (`data` may be null) Binds VBO after construction.
        {
            NewDataBytes(src, acc);
        }

        VertexArray(const VertexArray &) = delete;
        VertexArray(VertexArray &&) = delete;
        VertexArray &operator=(const VertexArray &) = delete;
        VertexArray &operator=(VertexArray &&) = delete;

        ~VertexArray()
        {
            if (InternalState::vbo_binding == vbo)
                InternalState::vbo_binding = 0;
            glDeleteBuffers(1, &vbo);
        }

        GLuint Handle() const
        {
            return vbo;
        }

        static void BindStorageHandle(GLuint id) // Binds a VBO.
        {
            if (id == InternalState::vbo_binding)
                return;
            InternalState::vbo_binding = id;
            glBindBuffer(GL_ARRAY_BUFFER, id);
            InternalState::vbo_attribs_configured = 0;
        }

        template <typename TT> static void BindHandle(GLuint id) // Binds a VBO and configures attrib pointers based on template parameter.
        {
            BindStorageHandle(id);
            if (!InternalState::vbo_attribs_configured)
            {
                InternalState::vbo_attribs_configured = 1;
                Attribs::SetForType<TT>();
            }
        }

        void BindStorage() const // Binds VBO.
        {
            BindStorageHandle(vbo);
        }
        static void BindStorageNothing() // Unbinds current VBO.
        {
            BindStorageHandle(0);
        }
        void Bind() const // Binds VBO and configures pointers.
        {
            BindHandle<T>(vbo);
        }
        static void BindNothing() // Unbinds current VBO and clears all attrib pointers.
        {
            BindHandle<void>(0);
        }

        void NewData(Utils::ArrayViewer<T> src, StorageType acc = StorageType::draw_static) // (`src` may be null) Auto binds VBO
        {
            BindStorage();
            glBufferData(GL_ARRAY_BUFFER, src.Size() * sizeof (T), src.Data(), (GLenum)acc);
        }
        WarningForMobile("This does not work on mobile platforms.")
        void Get(Utils::ArrayProxy<T> dst, unsigned int src_pos) const // Auto binds VBO
        {
            BindStorage();
            ForPC
            (
            glGetBufferSubData(GL_ARRAY_BUFFER, src_pos * sizeof (T), dst.Size() * sizeof (T), dst.Data());
            )
        }
        void Set(Utils::ArrayViewer<T> src, unsigned int dst_pos) // Auto binds VBO
        {
            BindStorage();
            glBufferSubData(GL_ARRAY_BUFFER, dst_pos * sizeof (T), src.Size() * sizeof (T), src.Data());
        }

        void NewDataBytes(Utils::ArrayViewer<uint8_t> src, StorageType acc = StorageType::draw_static) // (`src` may be null) Auto binds VBO
        {
            BindStorage();
            glBufferData(GL_ARRAY_BUFFER, src.Size(), src.Data(), (GLenum)acc);
        }
        WarningForMobile("This does not work on mobile platforms.")
        void GetBytes(Utils::ArrayProxy<uint8_t> dst, unsigned int src_pos) const // Auto binds VBO
        {
            BindStorage();
            ForPC
            (
            glGetBufferSubData(GL_ARRAY_BUFFER, src_pos, dst.Size(), dst.Data());
            )
        }
        void SetBytes(Utils::ArrayViewer<uint8_t> src, unsigned int dst_pos) // Auto binds VBO
        {
            BindStorage();
            glBufferSubData(GL_ARRAY_BUFFER, dst_pos, src.Size(), src.Data());
        }

        void DrawPoints   (unsigned int pos, unsigned int count) const {Bind(); glDrawArrays(GL_POINTS   , pos, count);} // Auto binds VBO
        void DrawLines    (unsigned int pos, unsigned int count) const {Bind(); glDrawArrays(GL_LINES    , pos, count);} // Auto binds VBO
        void DrawTriangles(unsigned int pos, unsigned int count) const {Bind(); glDrawArrays(GL_TRIANGLES, pos, count);} // Auto binds VBO

        void DrawPoints   (unsigned int count) const {DrawPoints   (0, count);} // Auto binds VBO
        void DrawLines    (unsigned int count) const {DrawLines    (0, count);} // Auto binds VBO
        void DrawTriangles(unsigned int count) const {DrawTriangles(0, count);} // Auto binds VBO
    };

    template <typename T>
    class SizedVertexArray : public VertexArray<T>
    {
        unsigned int size;

        SizedVertexArray(const SizedVertexArray &) = delete;
        SizedVertexArray(SizedVertexArray &&) = delete;
        SizedVertexArray &operator=(const SizedVertexArray &) = delete;
        SizedVertexArray &operator=(SizedVertexArray &&) = delete;

      public:
        SizedVertexArray()
        {
            size = 0;
        }
        SizedVertexArray(Utils::ArrayViewer<T> src, StorageType acc = StorageType::draw_static) : VertexArray<T>(src, acc) // (`data` may be null) Binds VBO after construction.
        {
            size = src.Size();
        }
        using VertexArray<T>::DrawPoints;
        using VertexArray<T>::DrawLines;
        using VertexArray<T>::DrawTriangles;
        void DrawPoints   () const {DrawPoints   (size);} // Auto binds VBO
        void DrawLines    () const {DrawLines    (size);} // Auto binds VBO
        void DrawTriangles() const {DrawTriangles(size);} // Auto binds VBO
    };


    template <typename L>
    class RenderingQueue
    {
        VertexArray<L> vao;
        uint32_t size;
        uint32_t pos;
        Utils::Array<L> arr;
        const char *name;

      public:
        RenderingQueue(const char *display_name, uint32_t l, StorageType acc = StorageType::draw_dynamic) : vao({(const uint8_t *)0, sizeof (L) * l}, acc) // Name MUST remain valid whlie a queue exists, you should use a string literal for that.
        {
            if (l == 0)
                Sys::Error("Invalid rendering queue size.");
            name = display_name;
            size = l;
            pos = 0;
            arr.Alloc(l);
        }
        RenderingQueue(const RenderingQueue &) = delete;
        RenderingQueue(RenderingQueue &&) = delete;
        RenderingQueue &operator=(const RenderingQueue &) = delete;
        RenderingQueue &operator=(RenderingQueue &&) = delete;

        ~RenderingQueue() {}

        uint32_t MaxSize() const
        {
            return size;
        }
        uint32_t CurrentSize() const
        {
            return pos;
        }

        std::size_t ByteSize() const
        {
            return arr.ByteSize();
        }

        void ChangeSize(uint32_t l, StorageType acc = StorageType::draw_dynamic)
        {
            if (l == 0)
                Sys::Error("Invalid rendering queue size.");
            size = l;
            pos = 0;
            delete [] arr;
            arr.Alloc(l);
            vao.~VertexArray();
            new(&vao) VertexArray<L>(0, sizeof (L) * l, acc);
        }

        L *Add(uint32_t amount)
        {
            if (pos + amount > size)
                Exception::RenderingQueueOverflow({name, Jo(size)});
            auto ret = arr + pos;
            pos += amount;
            return ret;
        }

        void Push1(const L &x)
        {
            if (pos >= size)
                Exception::RenderingQueueOverflow({name, Jo(size)});
            arr[pos    ] = x;
            pos += 1;
        }
        void Push2(const L &x, const L &y)
        {
            if (pos + 1 >= size)
                Exception::RenderingQueueOverflow({name, Jo(size)});
            arr[pos    ] = x;
            arr[pos + 1] = y;
            pos += 2;
        }
        void Push3(const L &x, const L &y, const L &z)
        {
            if (pos + 2 >= size)
                Exception::RenderingQueueOverflow({name, Jo(size)});
            arr[pos    ] = x;
            arr[pos + 1] = y;
            arr[pos + 2] = z;
            pos += 3;
        }
        void Push4as3x2(const L &x, const L &y, const L &z, const L &w) // a b d  b c d
        {
            if (pos + 5 >= size)
                Exception::RenderingQueueOverflow({name, Jo(size)});
            arr[pos    ] = x;
            arr[pos + 1] = y;
            arr[pos + 2] = w;
            arr[pos + 3] = y;
            arr[pos + 4] = z;
            arr[pos + 5] = w;
            pos += 6;
        }

        void Send() // You shall not call Push*() between Send() and Draw*()
        {
            vao.Set({arr, pos}, 0);
        }
        void Clear()
        {
            pos = 0;
        }

        void DrawPoints() const
        {
            vao.DrawPoints(0, pos);
        }
        void DrawLines() const
        {
            vao.DrawLines(0, pos);
        }
        void DrawTriangles() const
        {
            vao.DrawTriangles(0, pos);
        }
    };

    enum class Mirror
    {
        no = 0b00,
        x  = 0b01,
        y  = 0b10,
        xy = 0b11,
    };

    class ImageData
    {
        Utils::Array<u8vec4> data;
        ivec2 size;
      public:
        void LoadTGA(Utils::BinaryInput io, Mirror mirror = Mirror::no); // Can throw cant_open_file_read, file_parsing_failed and unexpected_eof.
        void SaveTGA(Utils::BinaryOutput io, Mirror mirror = Mirror::no); // Can throw cant_open_file_write.
        void LoadCompressed(Utils::BinaryInput io); // Can throw cant_open_file_read, file_parsing_failed and unexpected_eof.
        void SaveCompressed(Utils::BinaryOutput io); // Can throw cant_open_file_write.

        static ImageData FromTGA(Utils::BinaryInput io, Mirror mirror = Mirror::no)
        {
            ImageData ret;
            ret.LoadTGA(io.Move(), mirror);
            return ret;
        }
        static ImageData FromCompressed(Utils::BinaryInput io)
        {
            ImageData ret;
            ret.LoadCompressed(io.Move());
            return ret;
        }

        void Premultiply()
        {
            for (auto &it : data)
            {
                float f = float(it.a) / 255.0f;
                it.r = std::round(it.r * f);
                it.g = std::round(it.g * f);
                it.b = std::round(it.b * f);
            }
        }
        void Demultiply()
        {
            for (auto &it : data)
            {
                float f = float(it.a) / 255.0f;
                if (!std::isnormal(f))
                    continue;
                it.r = std::max((int)std::round(it.r / f), 255);
                it.g = std::max((int)std::round(it.g / f), 255);
                it.b = std::max((int)std::round(it.b / f), 255);
            }
        }
        void Empty(ivec2 new_size)
        {
            Clear();

            size = new_size;
            data.Alloc(size.product());
        }
        void LoadFromMem(ivec2 new_size, uint8_t *mem)
        {
            Empty(new_size);
            std::memcpy(data, mem, ByteSize());
        }
        void Clear()
        {
            size = {0,0};
            data.Free();
        }
        void *Data()
        {
            return data;
        }
        const void *Data() const
        {
            return data;
        }
        ivec2 Size() const
        {
            return size;
        }
        uint32_t ByteSize() const
        {
            return size.product() * sizeof (u8vec4);
        }
        u8vec4 &At(uvec2 pos)
        {
            return data[size.x * pos.y + pos.x];
        }
        u8vec4 At(uvec2 pos) const
        {
            return data[size.x * pos.y + pos.x];
        }
        ImageData()
        {
            size = {0,0};
        }
        ImageData(uvec2 new_size)
        {
            size = new_size;
            data.Alloc(size.product());
        }
        ~ImageData()
        {
            Clear();
        }
    };


    enum class WrapMode
    {
        clamp  = GL_CLAMP_TO_EDGE,
        mirror = GL_MIRRORED_REPEAT,
        repeat = GL_REPEAT,
        fill   = ForPC(GL_CLAMP_TO_BORDER) ForMobile(GL_CLAMP_TO_EDGE),
    };

    class Texture
    {
        int tex_id;
        GLuint handle;
        ivec2 size;
      public:
        Texture();
        ~Texture();
        Texture(const ImageData &data) : Texture() {SetData(data);}
        Texture(ivec2 size, void *ptr = 0) : Texture() {SetData(size, ptr);}
        static uint32_t MaxCount();
        static uint32_t CurrentCount();
        static void SetActiveSlot(unsigned int n) // You need to call this only if you use GL functions manually. Always use this instead of glActiveTexture() if you use this class!
        {
            static unsigned int active_tex = 0;
            if (n != active_tex)
            {
                glActiveTexture(GL_TEXTURE0 + n);
                active_tex = n;
            }
        }
        static void SlotSetUniform(GLint loc, unsigned int slot)
        {
            glUniform1i(loc, slot);
        }
        void Activate() const {SetActiveSlot(tex_id);}
        void SetData(const ImageData &data)
        {
            Activate();
            size = data.Size();
            glTexImage2D(GL_TEXTURE_2D, 0, ForPC(GL_RGBA8) ForMobile(GL_RGBA), size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data.Data());
        }
        void SetData(ivec2 new_size, void *ptr = 0)
        {
            Activate();
            size = new_size;
            glTexImage2D(GL_TEXTURE_2D, 0, ForPC(GL_RGBA8) ForMobile(GL_RGBA), size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, ptr);
        }
        void MinLinearInterpolation(bool n) {Activate(); glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, n ? GL_LINEAR : GL_NEAREST);} // This is 1 by default.
        void MagLinearInterpolation(bool n) {Activate(); glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, n ? GL_LINEAR : GL_NEAREST);}
        void LinearInterpolation(bool n)    {Activate(); glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, n ? GL_LINEAR : GL_NEAREST);
                                                         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, n ? GL_LINEAR : GL_NEAREST);}
        void WrapModeX(WrapMode mode) {Activate(); glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLuint)mode);}
        void WrapModeY(WrapMode mode) {Activate(); glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLuint)mode);}
        void WrapModeZ(WrapMode mode) {Activate(); ForPC(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, (GLuint)mode);)}
        void WrapModeXY(WrapMode mode)  {Activate(); glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLuint)mode);
                                                     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLuint)mode);}
        void WrapModeXYZ(WrapMode mode)  {Activate(); glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLuint)mode);
                                                      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLuint)mode);
                                                      ForPC(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, (GLuint)mode);)}
        GLuint Handle() const {return handle;}
        int Slot() const {return tex_id;}
        ivec2 Size() const {return size;}
    };

    namespace Internal
    {
        template <typename T> void SetUniform(GLint loc, const T &obj);
        #define LX_GEN_SINGLE(type, tag)       template <> P inline void SetUniform(GLint loc, const type &obj)            {C(glUniform1##tag(loc, obj);)}
        #define LX_GEN_VECTOR(type, tag, size) template <> P inline void SetUniform(GLint loc, const vec##size<type> &obj) {C(glUniform##size##tag##v(loc, 1, obj.as_array());)}
        #define LX_GEN_MATRIX(type, tag, size) template <> P inline void SetUniform(GLint loc, const mat##size<type> &obj) {C(glUniformMatrix##size##tag##v(loc, 1, 0, obj.as_array());)}
        #define C(...) __VA_ARGS__
        #define P
        LX_GEN_SINGLE(float,        f)
        LX_GEN_VECTOR(float,        f, 2)
        LX_GEN_VECTOR(float,        f, 3)
        LX_GEN_VECTOR(float,        f, 4)
        LX_GEN_SINGLE(int,          i)
        LX_GEN_VECTOR(int,          i, 2)
        LX_GEN_VECTOR(int,          i, 3)
        LX_GEN_VECTOR(int,          i, 4)
        LX_GEN_MATRIX(float, f, 2  )
        LX_GEN_MATRIX(float, f, 3  )
        LX_GEN_MATRIX(float, f, 4  )
        #undef C
        #undef P
        #define C(...) ForPC(__VA_ARGS__)
        #define P WarningForMobile("This does not work on mobile platforms (matrices with different dimensions and unsigned uniforms are not supported).")
        LX_GEN_SINGLE(unsigned int, ui)
        LX_GEN_VECTOR(unsigned int, ui, 2)
        LX_GEN_VECTOR(unsigned int, ui, 3)
        LX_GEN_VECTOR(unsigned int, ui, 4)
        LX_GEN_MATRIX(float, f, 2x3)
        LX_GEN_MATRIX(float, f, 2x4)
        LX_GEN_MATRIX(float, f, 3x2)
        LX_GEN_MATRIX(float, f, 3x4)
        LX_GEN_MATRIX(float, f, 4x2)
        LX_GEN_MATRIX(float, f, 4x3)
        #undef C
        #undef P
        #undef LX_GEN_SINGLE
        #undef LX_GEN_VECTOR
        #undef LX_GEN_MATRIX
        template <> inline void SetUniform(GLint loc, const Texture &ref) {SetUniform<GLint>(loc, ref.Slot());}
    }

    template <typename T> void SetUniform(GLint loc, const Utils::DisableDeduction<T> &obj) {Internal::SetUniform<T>(loc, obj);}

    struct ShaderSource
    {
        const char *vertex, *fragment;
    };

    class Shader
    {
        GLuint prog, vsh, fsh;
        Utils::Array<GLint> uniform_locs;

        GLint GetUniformLocation(unsigned int n) const
        {
            return uniform_locs[n];
        }
      public:
        Shader(const char *name, ShaderSource source, Utils::ArrayViewer<const char *> attribs, Utils::ArrayViewer<const char *> uniforms); // Can throw ShaderCompilationError and ShaderLinkingError.

        Shader(const Shader &) = delete;
        Shader(Shader &&) = delete;
        Shader &operator=(const Shader &) = delete;
        Shader &operator=(Shader &&) = delete;

        operator GLuint() const
        {
            return prog;
        }

        void Use() const
        {
            if (prog == InternalState::shader_binding)
                return;
            InternalState::shader_binding = prog;
            glUseProgram(prog);
        }
        static void UseNothing()
        {
            glUseProgram(0);
        }

        template <typename T> void SetUniform(int loc, const Utils::DisableDeduction<T> &obj, int offset = 0) const {Use(); Graphics::SetUniform<T>(GetUniformLocation(loc) + offset, obj);}

        ~Shader()
        {
            if (InternalState::shader_binding == prog)
                InternalState::shader_binding = 0;
            glDeleteShader(vsh);
            glDeleteShader(fsh);
            glDeleteProgram(prog);
        }
    };

    #if ForMobile(1) ForPC(0)
    #ifdef GL_DRAW_FRAMEBUFFER
    #undef GL_DRAW_FRAMEBUFFER
    #endif
    #define GL_DRAW_FRAMEBUFFER GL_FRAMEBUFFER
    #endif

    class Framebuffer
    {
        GLuint handle;
      public:
        static void BindBufferRead(GLuint id);
        static void BindBufferDraw(GLuint id);

        WarningForMobile("On mobile platforms this is replaced with BindDraw()!")
        void BindRead()
        {
            BindBufferRead(handle);
        }
        void BindDraw()
        {
            BindBufferDraw(handle);
        }
        WarningForMobile("On mobile platforms this is replaced with UnbindDraw()!")
        static void UnbindRead()
        {
            BindBufferRead(0);
        }
        static void UnbindDraw()
        {
            BindBufferDraw(0);
        }

        WarningForMobile("On mobile platforms this is replaced with BindDrawAndAttachTexture()!")
        void BindReadAndAttachTexture(const Texture &tex, unsigned int att_id = 0)
        {
            ForPC
            (
                BindRead();
                glFramebufferTexture(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + att_id, tex.Handle(), 0);
            )

            ForMobile
            (
                BindDrawAndAttachTexture(tex, att_id);
            )
        }
        void BindDrawAndAttachTexture(const Texture &tex, unsigned int att_id = 0)
        {
            BindDraw();

            ForPC
            (
                glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + att_id, tex.Handle(), 0);
            )

            ForMobile
            (
                glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + att_id, GL_TEXTURE_2D, tex.Handle(), 0);
            )
        }

        #if ForPC(1) ForMobile(0)
        void FillWithColor(u8vec4 color, unsigned int att_id = 0) // Auto binds for drawing
        {
            BindDraw();
            fvec4 true_color = color / 255.0f;
            glClearBufferfv(GL_COLOR, att_id, true_color.as_array());
        }
        void FillWithColor01(fvec4 color, unsigned int att_id = 0) // Auto binds for drawing
        {
            BindDraw();
            glClearBufferfv(GL_COLOR, att_id, color.as_array());
        }
        #endif


        void BindDrawAndClear()
        {
            BindDraw();
            glClear(GL_COLOR_BUFFER_BIT);
        }

        Framebuffer()
        {
            glGenFramebuffers(1, &handle);
            if (!handle)
                Sys::Error("Can't create a framebuffer.");
        }
        ~Framebuffer()
        {
            glDeleteFramebuffers(1, &handle);
        }
    };
}

#endif