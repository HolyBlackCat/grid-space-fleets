{fvec3 coords[]{
{0.000000,-11.000000,0.000000},
{0.000000,0.000000,11.000000},
{11.000000,0.000000,0.000000},
{0.000000,11.000000,0.000000},
{-11.000000,0.000000,0.000000},
{0.000000,0.000000,-11.000000}};
LOAD_TO = new std::remove_pointer<decltype(LOAD_TO)>::type({
#ifdef TRIANGLE
#undef TRIANGLE
#endif
#define TRIANGLE MATERIAL_PureWhite
TRIANGLE(coords[3-1],coords[2-1],coords[1-1]),
TRIANGLE(coords[4-1],coords[2-1],coords[3-1]),
TRIANGLE(coords[5-1],coords[2-1],coords[4-1]),
TRIANGLE(coords[1-1],coords[2-1],coords[5-1]),
TRIANGLE(coords[3-1],coords[1-1],coords[6-1]),
TRIANGLE(coords[4-1],coords[3-1],coords[6-1]),
TRIANGLE(coords[5-1],coords[4-1],coords[6-1]),
TRIANGLE(coords[1-1],coords[5-1],coords[6-1]),
#ifdef TRIANGLE
#undef TRIANGLE
#endif
#undef LOAD_TO
});}