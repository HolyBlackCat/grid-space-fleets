#include "tests.h"

namespace Tests
{
    void Triangle()
    {
        MarkLocation("Triangle Test");

        Graphics::Shader sh("Main",
{ForPC("#version 330") ForMobile("#version 100")
R"(
attribute vec2 a_pos;
attribute vec3 a_color;
varying vec3 v_color;
void main()
{
    gl_Position = vec4(a_pos, 0, 1);
    v_color = a_color;
})",
ForPC("#version 330") ForMobile("#version 100\nprecision mediump float;")
R"(
varying vec3 v_color;
void main()
{
    vec3 v;
    v.r = smoothstep(0.0,1.0,v_color.r);
    v.g = smoothstep(0.0,1.0,v_color.g);
    v.b = smoothstep(0.0,1.0,v_color.b);
    float maxval = v.r;
    if (v.g > maxval) maxval = v.g;
    if (v.b > maxval) maxval = v.b;
    gl_FragColor = vec4(v / maxval, 1);
})"}, {"a_pos", "a_color"}, {});
        sh.Use();

        using Layout = Graphics::TypePack<fvec2, fvec3>;
        Graphics::SizedVertexArray<Layout> va({{{0,0.75},{1,0,0}},{{0.75,-0.75},{0,1,0}},{{-0.75,-0.75},{0,0,1}}});

        while (1)
        {
            Sys::BeginFrame();
            Sys::Tick();
            va.DrawTriangles();
            Sys::EndFrame();
        }
    }
}