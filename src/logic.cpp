#include "logic.h"

#include <random>

void Boot();
void Menu();

namespace Game
{
    void ShipPlacement();
    void NetConfig();
    void World();
}

void PreInit()
{
    Sys::SetCurrentFunction(Boot);
    Sys::Config::app_name = "GsF";
    Sys::Config::window_name = "Grid space Fleets";
    Sys::Config::opengl_config = ForWindows("3.3C_1_8888*") ForMac("3.2*_1_8888*") ForMobile("2.0E_1_8888*");
    Sys::Config::window_maximize_at_startup = 1;
    Sys::Config::window_fullscreen_at_startup = 1;
    Sys::Config::window_min_size = {640,480};
}

namespace Textures
{
    #define TEXTURES_LIST \
        TEXTURE(gui,0,clamp) \
        TEXTURE(skybox_front,1,clamp) \
        TEXTURE(skybox_back,1,clamp) \
        TEXTURE(skybox_left,1,clamp) \
        TEXTURE(skybox_right,1,clamp) \
        TEXTURE(skybox_top,1,clamp) \
        TEXTURE(skybox_bottom,1,clamp) \

    #define TEXTURE(x,y,z) Graphics::Texture *x;
    TEXTURES_LIST
    #undef TEXTURE
}

namespace Sounds
{
    #define SOUNDS_LIST \
        SOUND(menu_click) \
        SOUND(menu_error) \
        SOUND(ship_rotation) \
        SOUND(set_target) \
        SOUND(remove_target) \
        SOUND(no_shots) \
        SOUND(end_turn) \
        SOUND(ship_shot) \
        SOUND(ship_hit) \
        SOUND(ship_dead) \
        SOUND(open_menu) \
        SOUND(key) \

    #define SOUND(name) Audio::Buffer *name;
    SOUNDS_LIST
    #undef SOUND
}

namespace ShaderSources
{
    Graphics::ShaderSource flat
    {
ForWindows("#version 330") ForMac("#version 150") ForMobile("#version 100") R"(
uniform mat4 u_matrix;
uniform vec2 u_texture_size;
attribute vec2 a_pos;
attribute vec2 a_texpos;
attribute vec4 a_color;
attribute vec2 a_fac;
varying vec2 v_texpos;
varying vec4 v_color;
varying vec2 v_fac;
void main()
{
    gl_Position = u_matrix * vec4(a_pos,0,1);
    // = vec4(a_pos,0,1) * u_matrix;
    v_texpos = a_texpos / u_texture_size;
    v_color = a_color;
    v_fac = a_fac;
})",
ForWindows("#version 330") ForMac("#version 150") ForMobile("#version 100") R"(
#ifdef GL_ES
precision mediump float;
#endif
uniform sampler2D u_texture;
varying vec2 v_texpos;
varying vec4 v_color;
varying vec2 v_fac;
void main()
{
    vec4 final_color = v_color * v_fac.xxxy + texture2D(u_texture,v_texpos) * (1-v_fac).xxxy;
    gl_FragColor = vec4(final_color.rgb * final_color.a, final_color.a);
})"
    };
    Graphics::ShaderSource gui
    {
        ForWindows("#version 330") ForMac("#version 150") ForMobile("#version 100") R"(
attribute vec2 a_pos;
varying vec2 v_pos;
void main()
{
    v_pos = a_pos;
    gl_Position = vec4(a_pos*2-1, 0, 1);
})",
ForWindows("#version 330") ForMac("#version 150") ForMobile("#version 100") R"(
#ifdef GL_ES
precision mediump float;
#endif
uniform sampler2D u_texture;
uniform vec2 u_texture_size;
uniform vec2 u_mouse_pos;
uniform float u_scale;
uniform bool u_mouse_exists;
varying vec2 v_pos;
void main()
{
    const float aberration = 1.15;
    float max_angle = asin(0.5);
    float min_cos = cos(max_angle);

    const float red_angle = 0;
    float pi_2_div_3 = acos(-0.5);

    float angle = asin(v_pos.x - 0.5);

    float depth = cos(angle) - min_cos + 1;
    vec2 src_pos;
    src_pos.x = angle / max_angle / 2 * depth + 0.5;
    src_pos.y = (v_pos.y - 0.5) * depth + 0.5;

    if (src_pos.x < 0 || src_pos.y < 0 || src_pos.x > 1 || src_pos.y > 1)
        discard;

    vec2 dir;
    vec2 den;
    if (u_mouse_exists)
    {
        dir = src_pos - u_mouse_pos / u_texture_size;
        den = (pow(length(dir*500),2)+800) * u_texture_size / 250;
        dir = normalize(dir);
    }
    else
    {
        dir = vec2(0,0);
        den = vec2(1,1);
    }

    vec4 c1 = texture2D(u_texture, src_pos+dir*1/den + aberration*vec2(cos(red_angle),sin(red_angle))/u_texture_size/u_scale);
    vec4 c2 = texture2D(u_texture, src_pos+dir*1/den + aberration*vec2(cos(red_angle+pi_2_div_3),sin(red_angle+pi_2_div_3))/u_texture_size/u_scale);
    vec4 c3 = texture2D(u_texture, src_pos+dir*1/den + aberration*vec2(cos(red_angle-pi_2_div_3),sin(red_angle-pi_2_div_3))/u_texture_size/u_scale);

    c1.rgb /= c1.a;
    c2.rgb /= c2.a;
    c3.rgb /= c3.a;

    vec4 final_color = vec4(0,0,0,((c1.a + c2.a + c3.a) / 3.0) * min(
                                                                     min(
                                                                         min(
                                                                             src_pos.x * u_texture_size.x / 4,
                                                                             src_pos.y * u_texture_size.y / 4),
                                                                         min(
                                                                             (1 - src_pos.x) * u_texture_size.x / 4,
                                                                             (1 - src_pos.y) * u_texture_size.y / 4)),
                                                                     1));
    /*
    final_color.gb += c1.gb / 2;
    final_color.rb += c2.rb / 2;
    final_color.rg += c3.rg / 2;*/
    final_color.r = c1.r;
    final_color.g = c2.g;
    final_color.b = c3.b;

    gl_FragColor = vec4(final_color.rgb * final_color.a, final_color.a);
})"
    };

    Graphics::ShaderSource world
    {
ForWindows("#version 330") ForMac("#version 150") ForMobile("#version 100") R"(
uniform mat4 u_matrix;
uniform mat4 u_model_matrix;
uniform vec3 u_cam_pos;
uniform bool u_enable_normals;
attribute vec3 a_pos;
attribute vec3 a_normal;
attribute vec2 a_texpos;
attribute vec4 a_color;
attribute vec2 a_fac;
attribute float a_texnum;
varying vec3 v_pos;
varying vec3 v_normal;
varying vec2 v_texpos;
varying vec4 v_color;
varying vec2 v_fac;
varying float v_texnum;
void main()
{
    gl_Position = u_matrix * vec4(a_pos,1);
    // = vec4(a_pos,1) * u_matrix;
    if (u_enable_normals)
    {
        v_pos = (u_model_matrix * vec4(a_pos,1)).xyz - u_cam_pos;
        // = (vec4(a_pos,1) * u_model_matrix).xyz - u_cam_pos;
        v_normal = normalize(mat3(u_model_matrix) * a_normal);
        // = normalize(a_normal * mat3(u_model_matrix));
    }
    v_texpos = a_texpos;
    v_color = a_color;
    v_fac = a_fac;
    v_texnum = a_texnum;
})",
ForWindows("#version 330") ForMac("#version 150") ForMobile("#version 100") R"(
#ifdef GL_ES
precision mediump float;
#endif
uniform sampler2D u_textures[7];
uniform bool u_enable_normals;
uniform vec4 u_color_factor;
varying vec3 v_pos;
varying vec3 v_normal;\
varying vec2 v_texpos;
varying vec4 v_color;
varying vec2 v_fac;
varying float v_texnum;
void main()
{
    vec4 final_color;

    switch (int(round(v_texnum)))
    {
        case 0: final_color = v_color * v_fac.xxxy + texture2D(u_textures[0],v_texpos) * (1-v_fac).xxxy; break;
        case 1: final_color = v_color * v_fac.xxxy + texture2D(u_textures[1],v_texpos) * (1-v_fac).xxxy; break;
        case 2: final_color = v_color * v_fac.xxxy + texture2D(u_textures[2],v_texpos) * (1-v_fac).xxxy; break;
        case 3: final_color = v_color * v_fac.xxxy + texture2D(u_textures[3],v_texpos) * (1-v_fac).xxxy; break;
        case 4: final_color = v_color * v_fac.xxxy + texture2D(u_textures[4],v_texpos) * (1-v_fac).xxxy; break;
        case 5: final_color = v_color * v_fac.xxxy + texture2D(u_textures[5],v_texpos) * (1-v_fac).xxxy; break;
        case 6: final_color = v_color * v_fac.xxxy + texture2D(u_textures[6],v_texpos) * (1-v_fac).xxxy; break;
    }

    if (final_color.a < 0.001)
        discard;

    if (u_enable_normals)
    {
        const vec3 dir_to_light_src = vec3(0,0,1);
        const float ambient_light = 0.5;
        float factor = degrees(acos(clamp(dot(-dir_to_light_src, v_normal),-1,1))) / 180 * (1 - ambient_light) + ambient_light;
        factor *= dot(-normalize(v_pos), v_normal) + 0.5;
        final_color.rgb *= factor;
    }

    final_color *= u_color_factor;
    gl_FragColor = vec4(final_color.rgb * final_color.a, final_color.a);
})"
    };
}

namespace Config
{
    static constexpr bool tex_load_compressed_instead_of_tga = 1;
    static constexpr bool tex_save_tga = 0;
    static constexpr bool tex_save_compressed = 0;


    static constexpr uint16_t network_port = 7777;


    static const std::initializer_list<ivec2> full_circle_offsets{{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1},{0,1}};

    static const std::initializer_list<ivec2> extended_circle_offsets{{1,0},{-1,0},{0,1},{0,-1},{1,1},{-1,-1},{1,-1},{-1,1}, {2,0},{-2,0},{0,2},{0,-2}, {2,2},{2,-2},{-2,-2},{-2,2}};

    static constexpr int fade_upper_cap = 60;

    static constexpr int menu_button_timer_upper_cap = 20;

    static constexpr int board_hor_offset = -40;

    static const float world_cell_selection_angle_cap = to_rad(30.0);

    static constexpr fvec4 menu_text_color{1,1,1,1},
                           menu_text_shadow_color{0,0.25,0.5,1},
                           menu_text_selected_color{1,0.5,0,1},
                           menu_text_selected_shadow_color{0.25,0.25,0.25,1};


    const Utils::Array<Utils::Array<Utils::Array<ivec2>>> ship_tile_models
    {
        { // 6
            {{0,0},{-1,0},{0,-1},{-1,-1},{-2,0},{1,-1}},
            {{0,0},{-1,0},{0,-1},{-1,-1},{0,-2},{-1,1}},
            {{0,0},{-1,0},{0,-1},{-1,-1},{-1,-2},{0,1}},
            {{0,0},{-1,0},{0,-1},{-1,-1},{-2,-1},{1,0}},
        },
        { // 5
            {{0,-2},{0,-1},{0,0},{0,1},{0,2}},
            {{-2,-2},{-1,-1},{0,0},{1,1},{2,2}},
            {{2,0},{1,0},{0,0},{-1,0},{-2,0}},
            {{2,-2},{1,-1},{0,0},{-1,1},{-2,2}},
        },
        { // 4
            {{0,0},{1,0},{-1,0},{0,-1}},
            {{0,0},{0,1},{-1,0},{0,-1}},
            {{0,0},{0,1},{1,0},{-1,0}},
            {{0,0},{0,1},{1,0},{0,-1}},
        },
        { // 3
            {{0,-1},{0,0},{0,1}},
            {{-1,-1},{0,0},{1,1}},
            {{1,0},{0,0},{-1,0}},
            {{1,-1},{0,0},{-1,1}},
        },
        { // 2
            {{0,0},{-1,0}},
            {{0,-1},{-1,0}},
            {{0,0},{0,-1}},
            {{0,0},{-1,-1}},
        },
    };
}

namespace Info
{
    std::mt19937 rng;

    ivec2 gui_texture_size;
    bool gui_scale;

    ivec2 GuiMousePos()
    {
        return ivec2(fvec2((Input::MousePos() - ((ivec2)Window::Size()+1)/2)) * fvec2(gui_texture_size) / fvec2(Window::Size()));
    }

    ivec2 GuiBoardMousePos() // Returns [-1;-1] if mouse is not on the board.
    {
        if (!(GuiMousePos()-ivec2{Config::board_hor_offset,0} >= -100) || !(GuiMousePos()-ivec2{Config::board_hor_offset,0} < 100))
            return {-1,-1};
        return (GuiMousePos()-ivec2{Config::board_hor_offset,0} + 100) / 10;
    }

    bool GuiMouseInRect(ivec2 pos, ivec2 size)
    {
        return (GuiMousePos() >= pos) && (GuiMousePos() < pos + size);
    }
}

namespace Shaders
{
    Graphics::Shader *flat;
    Graphics::Shader *gui;

    Graphics::Shader *world;

    fmat4 world_model_mat = fmat4::identity(),
          world_view_mat = fmat4::identity(),
          world_proj_mat = fmat4::identity();

    fvec3 world_cam_pos, actual_world_cam_pos;
    float world_cam_xa, world_cam_ya;

    void WorldComputeViewMatrix(fvec3 pos = world_cam_pos)
    {
        actual_world_cam_pos = pos;
        float xcos = std::cos(world_cam_xa),
              xsin = std::sin(world_cam_xa),
              ycos = std::cos(world_cam_ya),
              ysin = std::sin(world_cam_ya);
        world_view_mat = fmat4::look_at(pos, pos + fvec3{xcos*ycos,xsin*ycos,ysin}, fvec3{-xcos*ysin,-xsin*ysin,ycos});
    }

    void SendMatricesToWorldShader() // Don't forget to enable the shader first.
    {
        //fmat4 sum = world_model_mat;
        //sum = sum.mul(world_view_mat);
        //sum = sum.mul(world_proj_mat);
        fmat4 sum = world_proj_mat /mul/ world_view_mat /mul/ world_model_mat;
        world->SetUniform<fmat4>(0, sum);
        world->SetUniform<fmat4>(1, world_model_mat);
        world->SetUniform<fvec3>(3, actual_world_cam_pos);
        //glUniformMatrix4fv(world->GetUniformLocation(0), 1, 0, sum.as_array());
        //glUniformMatrix4fv(world->GetUniformLocation(1), 1, 0, world_model_mat.as_array());
        //glUniform3fv(world->GetUniformLocation(3), 1, actual_world_cam_pos.as_array());
    }

    void WorldEnableNormals(bool state) // Don't forget to enable the shader first.
    {
        world->SetUniform<int>(4, state);
        //glUniform1i(world->GetUniformLocation(4), state);
    }

    void WorldColorFactor(fvec3 fac = {1,1,1}, float alpha = 1)
    {
        world->SetUniform<fvec4>(5, {fac.r, fac.g, fac.b, alpha});
        //glUniform4f(Shaders::world->GetUniformLocation(5), fac.r, fac.g, fac.b, alpha);
    }

    namespace Cameras
    {
        void Observer(float sphere = 1, bool allow_movement = 1)
        {
            static constexpr float distance = 35;

            sphere = 3*sphere*sphere - 2*sphere*sphere*sphere;

            if (allow_movement)
            {
                world_cam_xa -= Input::MouseShift().x / 1000.0;
                world_cam_ya -= Input::MouseShift().y / 1000.0;
            }

            while (world_cam_xa < to_rad(-180.0)) world_cam_xa += to_rad(360.0);
            while (world_cam_xa > to_rad(180.0)) world_cam_xa -= to_rad(360.0);

            if (world_cam_ya > to_rad(90.0)) world_cam_ya = to_rad(90.0);
            else if (world_cam_ya < to_rad(-90.0)) world_cam_ya = to_rad(-90.0);

            float xcos = std::cos(world_cam_xa),
                  xsin = std::sin(world_cam_xa),
                  ycos = std::cos(world_cam_ya),
                  ysin = std::sin(world_cam_ya);

            if (sphere)
                world_cam_pos = -fvec3{xcos*ycos,xsin*ycos,ysin} * (distance * sphere);
        }

        void Debug()
        {
            static constexpr float movement_speed = 0.3;

            static fvec3 pos;
            static float ax, ay;

            ax -= Input::MouseShift().x / 1000.0;
            ay -= Input::MouseShift().y / 1000.0;

            while (ax < to_rad(-180.0)) ax += to_rad(360.0);
            while (ax > to_rad(180.0)) ax -= to_rad(360.0);
            if (ay > to_rad(90.0)) ay = to_rad(90.0);
            else if (ay < to_rad(-90.0)) ay = to_rad(-90.0);

            float xcos = std::cos(ax),
                  xsin = std::sin(ax);

            if (Input::KeyDown(Input::Key<'W'>()))
                pos += fvec3{xcos,xsin,0} * movement_speed;
            if (Input::KeyDown(Input::Key<'S'>()))
                pos -= fvec3{xcos,xsin,0} * movement_speed;
            if (Input::KeyDown(Input::Key<'D'>()))
                pos += fvec3{xsin,-xcos,0} * movement_speed;
            if (Input::KeyDown(Input::Key<'A'>()))
                pos -= fvec3{xsin,-xcos,0} * movement_speed;
            if (Input::KeyDown(Input::Key_Space()))
                pos.z += movement_speed;
            if (Input::KeyDown(Input::Key_Shift_L()))
                pos.z -= movement_speed;

            world_cam_pos = pos;
            world_cam_xa = ax;
            world_cam_ya = ay;
        }
    }

    void SendMousePosToGuiShader(bool mouse_exists = 1) // Don't forget to enable the shader first.
    {
        if (ForPC(1) ForMobile(0) && Input::MouseFocus() && mouse_exists)
        {
            gui->SetUniform<int>(4, 1);
            gui->SetUniform<fvec2>(2, fvec2(Info::GuiMousePos()*fvec2{1,-1}+fvec2(Info::gui_texture_size/2)));
            //glUniform1i(gui->GetUniformLocation(4), 1);
            //glUniform2fv(gui->GetUniformLocation(2), 1, fvec2(Info::GuiMousePos()*fvec2{1,-1}+fvec2(Info::gui_texture_size/2)).as_array());
        }
        else
            gui->SetUniform<int>(4, 0);
            //glUniform1i(gui->GetUniformLocation(4), 0);
            //Disabled: glUniform2fv(Shaders::gui->GetUniformLocation(2), 1, fvec2{100500,100500}.as_array());
    }
}

namespace Info
{
    ivec2 WorldBoardCameraPos() // Returns [-1;-1] if camera is not on the board.
    {
        if (Shaders::world_cam_xa < -Config::world_cell_selection_angle_cap || Shaders::world_cam_xa > Config::world_cell_selection_angle_cap)
            return {-1,-1};
        if (Shaders::world_cam_ya < -Config::world_cell_selection_angle_cap || Shaders::world_cam_ya > Config::world_cell_selection_angle_cap)
            return {-1,-1};

        ivec2 ret;
        ret.x = (-Shaders::world_cam_xa + Config::world_cell_selection_angle_cap) / Config::world_cell_selection_angle_cap * 10;
        ret.y = (-Shaders::world_cam_ya + Config::world_cell_selection_angle_cap) / Config::world_cell_selection_angle_cap * 10;

        ret.x = clamp(ret.x, 0, 19);
        ret.y = clamp(ret.y, 0, 19);
        return ret;
    }

    fvec2 WorldBoardCameraPosFloat() // Returns [-1;-1] if camera is not on the board.
    {
        if (Shaders::world_cam_xa < -Config::world_cell_selection_angle_cap || Shaders::world_cam_xa > Config::world_cell_selection_angle_cap)
            return {-1,-1};
        if (Shaders::world_cam_ya < -Config::world_cell_selection_angle_cap || Shaders::world_cam_ya > Config::world_cell_selection_angle_cap)
            return {-1,-1};

        fvec2 ret;
        ret.x = (-Shaders::world_cam_xa + Config::world_cell_selection_angle_cap) / Config::world_cell_selection_angle_cap * 10;
        ret.y = (-Shaders::world_cam_ya + Config::world_cell_selection_angle_cap) / Config::world_cell_selection_angle_cap * 10;

        ret.x = clamp<float>(ret.x, 0, 20);
        ret.y = clamp<float>(ret.y, 0, 20);
        return ret;
    }
}

namespace Framebuffers
{
    Graphics::Texture *gui_texture;
    Graphics::Framebuffer *gui;
}

namespace Models
{
    namespace GUI
    {
        Graphics::VertexArray<Layouts::GUI> *quad;
    }

    Graphics::SizedVertexArray<Layouts::World> *ship2;
    Graphics::SizedVertexArray<Layouts::World> *ship3;
    Graphics::SizedVertexArray<Layouts::World> *ship4;
    Graphics::SizedVertexArray<Layouts::World> *ship5;
    Graphics::SizedVertexArray<Layouts::World> *ship6;
    Graphics::SizedVertexArray<Layouts::World> *bullet;
    Graphics::SizedVertexArray<Layouts::World> *box;
    Graphics::SizedVertexArray<Layouts::World> *explosion;
}

namespace Queues
{
    Graphics::RenderingQueue<Layouts::Flat> *gui;

    Graphics::RenderingQueue<Layouts::World> *world;
}

class Locale final
{
    ~Locale() = delete;

    #define LOCALE_VARS_LIST \
        LOCALE(menu_0                  , "Singleplayer"                                          , "��������� ����"                                                            ) \
        LOCALE(menu_1                  , "Host"                                                  , "������� ������"                                                            ) \
        LOCALE(menu_2                  , "Connect"                                               , "������������"                                                              ) \
        LOCALE(menu_3                  , "Quit"                                                  , "�����"                                                                     ) \
        LOCALE(start_game              , "START\nGAME"                                           , "������\n����"                                                              ) \
        LOCALE(ship_rotation_hint      , "Press RMB to rotate the ship"                          , "������� ���, ����� ��������� �������"                                      ) \
        LOCALE(rem_shots               , "Remaining shots"                                       , "�������� ���������"                                                        ) \
        LOCALE(end_turn_hint           , "Hold RMB to end the turn"                              , "����������� ���, ����� ��������� ���"                                      ) \
        LOCALE(spend_your_shots        , "You should use all your shots\nbefore making a turn"   , "��� ����� ��������� ��� ���� ��������\n����� ���, ��� ������ ���"          ) \
        LOCALE(victory                 , "VICTORY"                                               , "������"                                                                    ) \
        LOCALE(defeat                  , "DEFEAT"                                                , "���������"                                                                 ) \
        LOCALE(any_key                 , "Press any key to continue"                             , "������� ����� �������, ����� ����������"                                   ) \
        LOCALE(first_turn_hint         , "Use LMB to select targets.\nDestroy all enemy ships!"  , "����������� ���, ����� �������� ����.\n���������� ��� ������� ����������!" ) \
        LOCALE(game_menu_continue      , "Continue"                                              , "����������"                                                                ) \
        LOCALE(game_menu_quit          , "Quit"                                                  , "�����"                                                                     ) \
        LOCALE(cant_create_server      , "Can't create a server on port "                        , "�� ���� ������� ������ �� ����� "                                          ) \
        LOCALE(waiting_for_client      , "Waiting for an opponent."                              , "������� ���������."                                                        ) \
        LOCALE(port                    , "Port: "                                                , "����: "                                                                    ) \
        LOCALE(ip                      , "IP address:"                                           , "IP �����:"                                                                 ) \
        LOCALE(waiting_for_enemy_board , "Waiting for the opponent to finish placing his ships"  , "����, ���� �������� �������� ���� �������"                                 ) \
        LOCALE(waiting_for_enemy_shots , "Waiting for the opponent..."                           , "���� ���������..."                                                         ) \

    #define LOCALE(name, eng, rus) static const char *name##_VAL;
    LOCALE_VARS_LIST
    #undef LOCALE

    static bool eng;

  public:
    #define LOCALE(name, eng, rus) static const char *name() {return name##_VAL;}
    LOCALE_VARS_LIST
    #undef LOCALE

    static void Switch()
    {
        eng = !eng;

        if (eng)
        {
            #define LOCALE(name, eng, rus) name##_VAL = eng;
            LOCALE_VARS_LIST
            #undef LOCALE
        }
        else
        {
            #define LOCALE(name, eng, rus) name##_VAL = rus;
            LOCALE_VARS_LIST
            #undef LOCALE
        }
    }

    static bool English() {return eng;}
};
bool Locale::eng = 1;
#define LOCALE(name, eng, rus) const char *Locale::name##_VAL = eng;
LOCALE_VARS_LIST
#undef LOCALE

namespace Draw
{
    enum class Align : int
    {
        left   = 0,
        center = 1,
        right  = 2,
    };

    namespace GUI
    {
        void Rect(ivec2 pos, ivec2 sz, ivec2 texpos, ivec2 texsz, fvec4 color, fvec2 colorfac)
        {
            sz += pos;
            texsz += texpos;
            Queues::gui->Push4as3x2({fvec2{float(pos.x), float(sz .y)}, fvec2{float(texpos.x), float(texsz .y)}, color, colorfac},
                                    {fvec2{float(sz .x), float(sz .y)}, fvec2{float(texsz .x), float(texsz .y)}, color, colorfac},
                                    {fvec2{float(sz .x), float(pos.y)}, fvec2{float(texsz .x), float(texpos.y)}, color, colorfac},
                                    {fvec2{float(pos.x), float(pos.y)}, fvec2{float(texpos.x), float(texpos.y)}, color, colorfac});
        }
        void Rect90(ivec2 pos, ivec2 sz, ivec2 texpos, ivec2 texsz, fvec4 color, fvec2 colorfac)
        {
            sz += pos;
            texsz += texpos;
            Queues::gui->Push4as3x2({fvec2{float(pos.x), float(sz .y)}, fvec2{float(texsz .x), float(texsz .y)}, color, colorfac},
                                    {fvec2{float(sz .x), float(sz .y)}, fvec2{float(texsz .x), float(texpos.y)}, color, colorfac},
                                    {fvec2{float(sz .x), float(pos.y)}, fvec2{float(texpos.x), float(texpos.y)}, color, colorfac},
                                    {fvec2{float(pos.x), float(pos.y)}, fvec2{float(texpos.x), float(texsz .y)}, color, colorfac});
        }
        void Rect180(ivec2 pos, ivec2 sz, ivec2 texpos, ivec2 texsz, fvec4 color, fvec2 colorfac)
        {
            sz += pos;
            texsz += texpos;
            Queues::gui->Push4as3x2({fvec2{float(pos.x), float(sz .y)}, fvec2{float(texsz .x), float(texpos.y)}, color, colorfac},
                                    {fvec2{float(sz .x), float(sz .y)}, fvec2{float(texpos.x), float(texpos.y)}, color, colorfac},
                                    {fvec2{float(sz .x), float(pos.y)}, fvec2{float(texpos.x), float(texsz .y)}, color, colorfac},
                                    {fvec2{float(pos.x), float(pos.y)}, fvec2{float(texsz .x), float(texsz .y)}, color, colorfac});
        }
        void Rect270(ivec2 pos, ivec2 sz, ivec2 texpos, ivec2 texsz, fvec4 color, fvec2 colorfac)
        {
            sz += pos;
            texsz += texpos;
            Queues::gui->Push4as3x2({fvec2{float(pos.x), float(sz .y)}, fvec2{float(texpos.x), float(texpos.y)}, color, colorfac},
                                    {fvec2{float(sz .x), float(sz .y)}, fvec2{float(texpos.x), float(texsz .y)}, color, colorfac},
                                    {fvec2{float(sz .x), float(pos.y)}, fvec2{float(texsz .x), float(texsz .y)}, color, colorfac},
                                    {fvec2{float(pos.x), float(pos.y)}, fvec2{float(texsz .x), float(texpos.y)}, color, colorfac});
        }

        void Rect(ivec2 pos, ivec2 sz, ivec2 texpos, ivec2 texsz) {Rect(pos, sz, texpos, texsz, {0,0,0,0}, {0,0});}
        void Rect(ivec2 pos, ivec2 sz, ivec2 texpos)              {Rect(pos, sz, texpos, sz, {0,0,0,0}, {0,0});}
        void Rect90(ivec2 pos, ivec2 sz, ivec2 texpos, ivec2 texsz) {Rect90(pos, sz, texpos, texsz, {0,0,0,0}, {0,0});}
        void Rect90(ivec2 pos, ivec2 sz, ivec2 texpos)              {Rect90(pos, sz, texpos, sz, {0,0,0,0}, {0,0});}
        void Rect180(ivec2 pos, ivec2 sz, ivec2 texpos, ivec2 texsz) {Rect180(pos, sz, texpos, texsz, {0,0,0,0}, {0,0});}
        void Rect180(ivec2 pos, ivec2 sz, ivec2 texpos)              {Rect180(pos, sz, texpos, sz, {0,0,0,0}, {0,0});}
        void Rect270(ivec2 pos, ivec2 sz, ivec2 texpos, ivec2 texsz) {Rect270(pos, sz, texpos, texsz, {0,0,0,0}, {0,0});}
        void Rect270(ivec2 pos, ivec2 sz, ivec2 texpos)              {Rect270(pos, sz, texpos, sz, {0,0,0,0}, {0,0});}

        void RectFill(ivec2 pos, ivec2 sz, fvec4 color)
        {
            Rect(pos, sz, {0,0}, {0,0}, color, {1,1});
        }
        void LineH(ivec2 pos, int len, fvec4 color)
        {
            RectFill(pos, {len,1}, color);
        }
        void LineV(ivec2 pos, int len, fvec4 color)
        {
            RectFill(pos, {1,len}, color);
        }
        void RectFrame(ivec2 pos, ivec2 size, fvec4 color, int margin = 0)
        {
            LineH({pos.x+margin, pos.y}, size.x-margin*2, color);
            LineV({pos.x, pos.y+margin}, size.y-margin*2, color);
            LineH({pos.x+margin, pos.y+size.y-1}, size.x-margin*2, color);
            LineV({pos.x+size.x-1, pos.y+margin}, size.y-margin*2, color);
        }

        void Cursor()
        {
            if (ForPC(0) ForMobile(1))
                return;
            if (Input::MouseFocus())
                Draw::GUI::Rect(Info::GuiMousePos()-1,{16,16},{96,0},{16,16},{0,0,0,0},{0,0});
        }

        void LocaleSwitch() // Can be clicked.
        {
            if (Input::MouseButtonPressed(1) && Info::GuiMouseInRect(Info::gui_texture_size / 2 - 33, {32,32}))
            {
                Locale::Switch();
                Sounds::menu_click->Play();

                FILE *fptr = std::fopen("locale", "w");
                if (fptr)
                {
                    std::putc(Locale::English() ? '0' : '1', fptr);
                    std::fclose(fptr);
                }
            }

            if ((Info::GuiMousePos() >= Info::gui_texture_size / 2 - 32) && (Info::GuiMousePos() < Info::gui_texture_size / 2))
                Rect(Info::gui_texture_size / 2 - 33, {32,32}, {128-32*Locale::English(),48});
            else
                Rect(Info::gui_texture_size / 2 - 33, {32,32}, {128-32*Locale::English(),16});
        }

        void Text(const char *txt, ivec2 pos, Align align, fvec4 color = {1,1,1,1})
        {
            auto Len = [&](const char *txt) -> int
            {
                if (align == Align::left)
                    return 0;
                int ret = 0;
                while (*txt && *txt != '\n')
                    ret++, txt++;
                if (align == Align::center)
                    return ret * 3;
                else
                    return ret * 6;
            };

            float alpha = 1 - color.a;
            color.a = 0;

            ivec2 cur_pos = pos;
            cur_pos.x -= Len(txt);

            while (*txt)
            {
                if (*txt == '\n')
                {
                    cur_pos.x = pos.x - Len(txt+1);
                    cur_pos.y += 12;
                }
                else
                {
                    Rect(cur_pos, {6,12}, {*(unsigned char *)txt % 16 * 6, *(unsigned char *)txt / 16 * 12}, {6,12}, color, {1,alpha});
                    cur_pos.x += 6;
                }

                txt++;
            }
        }
        void TextL(const char *txt, ivec2 pos, fvec4 color = {1,1,1,1})
        {
            Text(txt, pos, Align::left, color);
        }
        void TextC(const char *txt, ivec2 pos, fvec4 color = {1,1,1,1})
        {
            Text(txt, pos, Align::center, color);
        }
        void TextR(const char *txt, ivec2 pos, fvec4 color = {1,1,1,1})
        {
            Text(txt, pos, Align::right, color);
        }
        void TextLarge(const char *txt, ivec2 pos, Align align, fvec4 color = {1,1,1,1})
        {
            auto Len = [&](const char *txt) -> int
            {
                if (align == Align::left)
                    return 0;
                int ret = 0;
                while (*txt && *txt != '\n')
                    ret++, txt++;
                if (align == Align::center)
                    return ret * 6 + ret / 2;
                else
                    return ret * 13;
            };

            float alpha = 1 - color.a;
            color.a = 0;

            ivec2 cur_pos = pos;
            cur_pos.x -= Len(txt);

            while (*txt)
            {
                if (*txt == '\n')
                {
                    cur_pos.x = pos.x - Len(txt+1);
                    cur_pos.y += 18;
                }
                else
                {
                    Rect(cur_pos, {13,18}, {*(unsigned char *)txt % 16 * 13, *(unsigned char *)txt / 16 * 18 + 192}, {13,18}, color, {1,alpha});
                    cur_pos.x += 13;
                }

                txt++;
            }
        }
        void TextLargeL(const char *txt, ivec2 pos, fvec4 color = {1,1,1,1})
        {
            TextLarge(txt, pos, Align::left, color);
        }
        void TextLargeC(const char *txt, ivec2 pos, fvec4 color = {1,1,1,1})
        {
            TextLarge(txt, pos, Align::center, color);
        }
        void TextLargeR(const char *txt, ivec2 pos, fvec4 color = {1,1,1,1})
        {
            TextLarge(txt, pos, Align::right, color);
        }

        void ShipGrid()
        {
            static constexpr int corners_displacement = -109, sides_displacement = -102, dots_displacement = -50;

            //Rect({-100,-100},{199,199},{208,192});

            RectFrame({Config::board_hor_offset-102,-102},{203,203},{1,1,1,1},24);

            Rect({Config::board_hor_offset+corners_displacement, corners_displacement},{32,32},{160,0});
            Rect90({Config::board_hor_offset-corners_displacement-33, corners_displacement},{32,32},{160,0});
            Rect180({Config::board_hor_offset-corners_displacement-33, -corners_displacement-33},{32,32},{160,0});
            Rect270({Config::board_hor_offset+corners_displacement, -corners_displacement-33},{32,32},{160,0});

            Rect(ivec2{Config::board_hor_offset, sides_displacement}-16,{32,32},{160,32});
            Rect90(ivec2{Config::board_hor_offset-sides_displacement-1, 0}-16,{32,32},{160,32});
            Rect180(ivec2{Config::board_hor_offset-1, -sides_displacement-1}-16,{32,32},{160,32});
            Rect270(ivec2{Config::board_hor_offset+sides_displacement, -1}-16,{32,32},{160,32});

            Rect({Config::board_hor_offset-12,-12}, {24,24}, {96,80});

            Rect(ivec2{Config::board_hor_offset+dots_displacement,0}-12, {24,24}, {120,80});
            Rect90(ivec2{Config::board_hor_offset-1,dots_displacement}-12, {24,24}, {120,80});
            Rect(ivec2{Config::board_hor_offset-dots_displacement,0}-12, {24,24}, {120,80});
            Rect90(ivec2{Config::board_hor_offset-1,-dots_displacement}-12, {24,24}, {120,80});

            Rect(ivec2{Config::board_hor_offset+dots_displacement,dots_displacement}-8, {16,16}, {128,0});
            Rect(ivec2{Config::board_hor_offset-dots_displacement,dots_displacement}-8, {16,16}, {128,0});
            Rect(ivec2{Config::board_hor_offset+dots_displacement,-dots_displacement}-8, {16,16}, {128,0});
            Rect(ivec2{Config::board_hor_offset-dots_displacement,-dots_displacement}-8, {16,16}, {128,0});

            Rect(ivec2{Config::board_hor_offset+dots_displacement-8,corners_displacement}, {16,16}, {144,0});
            Rect(ivec2{Config::board_hor_offset-dots_displacement-8,corners_displacement}, {16,16}, {144,0});
            Rect90(ivec2{Config::board_hor_offset-sides_displacement-10,dots_displacement-8}, {16,16}, {144,0});
            Rect90(ivec2{Config::board_hor_offset-sides_displacement-10,-dots_displacement-8}, {16,16}, {144,0});
            Rect180(ivec2{Config::board_hor_offset+dots_displacement-9,-sides_displacement-10}, {16,16}, {144,0});
            Rect180(ivec2{Config::board_hor_offset-dots_displacement-9,-sides_displacement-10}, {16,16}, {144,0});
            Rect270(ivec2{Config::board_hor_offset+corners_displacement,dots_displacement-9}, {16,16}, {144,0});
            Rect270(ivec2{Config::board_hor_offset+corners_displacement,-dots_displacement-9}, {16,16}, {144,0});
        }

        void ShipGridSquare(ivec2 pos, fvec4 color, int texture)
        {
            ivec2 board_corner{Config::board_hor_offset-101,-101};
            Rect(board_corner+pos*10, {11,11}, {192,texture*11}, {11,11}, color.to_vec3().to_vec4(), {1, 1-color.a});
        }

        void NumberLarge(unsigned int num, ivec2 pos, Align align, fvec4 color = {1,1,1,1})
        {
            float alpha = 1 - color.a;
            color.a = 0;
            const char *str = Jo(num);
            pos.x -= (int)align * 6 * std::strlen(str);
            while (*str)
            {
                Rect(pos, {12,20}, {12 * (*str - '0'),480}, {12,20}, color, {1,alpha});
                pos.x += 11;
                str++;
            }
        }
        void NumberLargeL(unsigned int num, ivec2 pos, fvec4 color = {1,1,1,1})
        {
            NumberLarge(num, pos, Align::left, color);
        }
        void NumberLargeC(unsigned int num, ivec2 pos, fvec4 color = {1,1,1,1})
        {
            NumberLarge(num, pos, Align::center, color);
        }
        void NumberLargeR(unsigned int num, ivec2 pos, fvec4 color = {1,1,1,1})
        {
            NumberLarge(num, pos, Align::right, color);
        }
    }

    namespace World
    {
        void Triangle(fvec3 a, fvec3 b, fvec3 c, fvec2 ta, fvec2 tb, fvec2 tc, fvec4 color, fvec2 fac, float texture = 0)
        {
            fvec3 normal = (a - c).cross(b - c);
            Queues::world->Push3({a, normal, ta, color, fac, texture},
                                 {b, normal, tb, color, fac, texture},
                                 {c, normal, tc, color, fac, texture});
        }

        void Quad(fvec3 a, fvec3 b, fvec3 c, fvec3 d, fvec2 ta, fvec2 tb, fvec2 tc, fvec2 td, fvec4 color, fvec2 fac, float texture = 0)
        {
            Triangle(a,b,c,ta,tb,tc,color,fac,texture);
            Triangle(c,d,a,tc,td,ta,color,fac,texture);
        }

        void Quad(fvec3 src, fvec3 x, fvec3 y, fvec2 tsrc, fvec2 tx, fvec2 ty, fvec4 color, fvec2 fac, float texture = 0)
        {
            Quad(src, src+x, src+x+y, src+y, tsrc, tsrc+tx, tsrc+tx+ty, tsrc+ty, color, fac, texture);
        }

        void Skybox()
        {
            static constexpr float s = 5;
            Quad({ s, s, s}, { s,-s, s}, { s,-s,-s}, { s, s,-s}, {0,0}, {1,0}, {1,1}, {0,1}, {0,0,0,0}, {0,0}, 1);
            Quad({-s, s, s}, {-s,-s, s}, {-s,-s,-s}, {-s, s,-s}, {1,0}, {0,0}, {0,1}, {1,1}, {0,0,0,0}, {0,0}, 2);
            Quad({ s, s, s}, {-s, s, s}, {-s, s,-s}, { s, s,-s}, {1,0}, {0,0}, {0,1}, {1,1}, {0,0,0,0}, {0,0}, 3);
            Quad({ s,-s, s}, {-s,-s, s}, {-s,-s,-s}, { s,-s,-s}, {0,0}, {1,0}, {1,1}, {0,1}, {0,0,0,0}, {0,0}, 4);
            Quad({ s, s, s}, { s,-s, s}, {-s,-s, s}, {-s, s, s}, {1,1}, {1,0}, {0,0}, {0,1}, {0,0,0,0}, {0,0}, 5);
            Quad({ s, s,-s}, { s,-s,-s}, {-s,-s,-s}, {-s, s,-s}, {1,0}, {1,1}, {0,1}, {0,0}, {0,0,0,0}, {0,0}, 6);
        }
    }
}


class Board
{
    /** Cell codes:
          0   - empty
          1   - dot
          2-5 - ship cell
    */

    Board(const Board &) = delete;
    Board(Board &&) = delete;
    Board &operator=(const Board &) = delete;
    Board &operator=(Board &&) = delete;

    uint8_t packet_id; // For networking purposes

    int array[20][20];

    float ship_angles[5];
    bool flip_ship6;

  public:
    fvec2 ship_positions[5];

    Board()
    {
        packet_id = 1;
        Reset();
    }

    int &At(ivec2 pos) // Returns 1 if out of range.
    {
        if (!(pos >= 0) || !(pos < 20))
        {
            static int ret;
            ret = 1;
            return ret;
        }
        return array[pos.y][pos.x];
    }

    int At(ivec2 pos) const
    {
        if (!(pos >= 0) || !(pos < 20))
        {
            static int ret;
            ret = 1;
            return ret;
        }
        return array[pos.y][pos.x];
    }

    void DeleteUnusedDot(ivec2 pos)
    {
        for (const ivec2 &offset : Config::full_circle_offsets)
        {
            if (At(pos+offset) > 1)
                return;
        }
        At(pos) = 0;
    }

    void DeleteShipAt(ivec2 pos)
    {
        if (At(pos) <= 1)
            return;
        At(pos) = 0;
        for (const ivec2 &offset : Config::full_circle_offsets)
        {
            if (At(pos+offset) == 1)
                DeleteUnusedDot(pos+offset);
            else
                DeleteShipAt(pos+offset);
        }
    }

    void Reset()
    {
        for (auto &it : array)
            for (auto &jt : it)
                jt = 0;
    }


    bool CanPlaceShipAt(int what_ship, ivec2 pos, int rot) const
    {
        for (const ivec2 &offset : Config::ship_tile_models[what_ship][rot])
            if (At(pos + offset) != 0)
                return 0;
        return 1;
    }

    void PlaceShipAt(int what_ship, ivec2 pos, int rot)
    {
        for (const ivec2 &offset : Config::ship_tile_models[what_ship][rot])
        {
            for (const ivec2 &dot_offset : Config::full_circle_offsets)
                At(pos + offset + dot_offset) = 1;
        }
        for (const ivec2 &offset : Config::ship_tile_models[what_ship][rot])
            At(pos + offset) = 6 - what_ship;

        ship_positions[what_ship] = pos - 9.5f;

        switch (what_ship) // Computing angles + special cases
        {
          case 0: // 6
            ship_angles[what_ship] = to_rad((rot + 1) / 2 * 90.0);
            ship_positions[what_ship] -= 0.5f;
            flip_ship6 = rot % 2 == 1;
            break;
          case 1: // 5
            ship_angles[what_ship] = to_rad(rot * 45.0);
            break;
          case 2: // 4
            ship_angles[what_ship] = to_rad(rot * 90.0);
            break;
          case 3: // 3
            ship_angles[what_ship] = to_rad(rot * 45.0);
            break;
          case 4: // 2
            ship_angles[what_ship] = to_rad(rot * 45.0);
            switch (rot)
            {
              case 0:
                ship_positions[what_ship].x -= 0.5f;
                break;
              case 2:
                ship_positions[what_ship].y -= 0.5f;
                break;
              default:
                ship_positions[what_ship] -= 0.5f;
                break;
            }
        };
    }

    void RandomizeShips()
    {
        Reset();

        for (int i = 0; i < 5; i++)
        {
            int rot = Info::rng() % Config::ship_tile_models[i].Size();

            ivec2 min{9000,9000}, max{-9000,-9000};

            for (const ivec2 &offset : Config::ship_tile_models[i][rot])
            {
                if (offset.x < min.x) min.x = offset.x;
                else if (offset.x > max.x) max.x = offset.x;
                if (offset.y < min.y) min.y = offset.y;
                else if (offset.y > max.y) max.y = offset.y;
            }

            min = -min;
            ivec2 size = ivec2{20,20} + min - max;

            while (1)
            {
                ivec2 cur_pos = ivec2{int(Info::rng()) % size.x, int(Info::rng()) % size.y} - min;
                if (CanPlaceShipAt(i, cur_pos, rot))
                {
                    PlaceShipAt(i, cur_pos, rot);
                    break;
                }
            }
        }
    }

    void Render()
    {
        for (int y = 0; y < 20; y++)
            for (int x = 0; x < 20; x++)
                if (array[y][x] != 0)
                    Draw::GUI::ShipGridSquare({x,y}, {0.9,0.9,0.9,1}, array[y][x] == 1 ? 1 : 0);
    }

    void Render3D(float grid_alpha = 1, ivec2 selection_rect = {-1,-1}, const std::vector<ivec2> targets = {}, int (*hitmap)[20][20] = 0, int (*health)[5] = 0, bool draw_only_dead_ships = 0, bool draw_dots = 0, float alive_ships_alpha = 1)
    {
        // Ships

        fmat4 saved_matrix = Shaders::world_model_mat;
        Shaders::world_model_mat = fmat4::rotate_with_normalized_axis({1,0,0}, to_rad(90.0)) /mul/ fmat4::dia({1.f/12, 1.f/12, 1.f/12});
        //Shaders::world_model_mat = fmat4::dia({1.f/12, 1.f/12, 1.f/12}).mul(fmat4::rotate_with_normalized_axis({1,0,0}, to_rad(90.0)));
        fmat4 tmp_matrix = Shaders::world_model_mat;

        const Graphics::SizedVertexArray<Layouts::World> *const models[5] {Models::ship6,
                                                                           Models::ship5,
                                                                           Models::ship4,
                                                                           Models::ship3,
                                                                           Models::ship2};
        Shaders::WorldEnableNormals(1);
        for (int i = 0; i < 5; i++)
        {
            if (draw_only_dead_ships && health && ((*health)[i]) != 0)
                continue;

            if (health && (*health)[i] == 0)
                Shaders::WorldColorFactor({0.25,0.25,0.25});
            else
                Shaders::WorldColorFactor({1,1,1}, alive_ships_alpha);

            Shaders::world_model_mat = fmat4::rotate_with_normalized_axis({0,0,-1}, pi<float>()-ship_angles[i]) /mul/ Shaders::world_model_mat;
            if (i == 0 && !flip_ship6)
                Shaders::world_model_mat = fmat4::rotate_with_normalized_axis({0,1,0}, pi<float>()) /mul/ Shaders::world_model_mat;
            Shaders::world_model_mat = fmat4::translate(fvec3{ship_positions[i].x, -ship_positions[i].y, 0}) /mul/ Shaders::world_model_mat;
            Shaders::world_model_mat = saved_matrix /mul/ Shaders::world_model_mat;
            /*
            Shaders::world_model_mat = Shaders::world_model_mat.mul(fmat4::rotate_with_normalized_axis({0,0,-1}, pi<float>()-ship_angles[i]));
            if (i == 0 && !flip_ship6)
                Shaders::world_model_mat = Shaders::world_model_mat.mul(fmat4::rotate_with_normalized_axis({0,1,0}, pi<float>()));
            Shaders::world_model_mat = Shaders::world_model_mat.mul(fmat4::translate(fvec3{ship_positions[i].x, -ship_positions[i].y, 0}));
            Shaders::world_model_mat = Shaders::world_model_mat.mul(saved_matrix);*/

            Shaders::SendMatricesToWorldShader();
            models[i]->DrawTriangles();
            Shaders::world_model_mat = tmp_matrix;
        }
        Shaders::WorldEnableNormals(0);

        Shaders::world_model_mat = saved_matrix;
        Shaders::SendMatricesToWorldShader();

        Shaders::WorldColorFactor();







        // Grid
        if (grid_alpha != 0)
        {
            Queues::world->Clear();

            static constexpr fvec3 f_x{-1,1,1}, f_y{1,-1,1}, f_xy{-1,-1,1};
            static constexpr float corners_dist = 7.25;

            /*/
            "Debug";
            static constexpr fvec3 cell_size{4/9.f,4/9.f,0};
            for (int y = 0; y < 20; y++)
            {
                for (int x = 0; x < 20; x++)
                {
                    fvec3 center = {x-9.5f, -(y-9.5f), 0};
                    if (At({x,y}) > 1)
                        Draw::World::Quad(center+cell_size, center+cell_size*f_x, center+cell_size*f_xy, center+cell_size*f_y, {},{},{},{}, {1,1,1,0.05}, {1,1});
                }
            }
            //*/

            // Targets
            for (const auto &it : targets)
            {
                fvec3 center = {it.x-9.5f-0.55f, -(it.y-9.5f+0.55f), 0};

                Draw::World::Quad(center, {1.1,0,0}, {0,1.1,0}, fvec2{192,44}/1024, fvec2{11,0}/1024, fvec2{0,11}/1024, {1,1,1,0}, {1,0});
            }


            // Corners
            Draw::World::Quad(fvec3{corners_dist,corners_dist,0},      fvec3{3.2,0,0},      fvec3{0,3.2,0},      fvec2{192,96}/1024, fvec2{-32,0}/1024, fvec2{0,-32}/1024, {0,0,0,0}, {0,1-grid_alpha});
            Draw::World::Quad(fvec3{corners_dist,corners_dist,0}*f_x,  fvec3{3.2,0,0}*f_x,  fvec3{0,3.2,0}*f_x,  fvec2{192,96}/1024, fvec2{-32,0}/1024, fvec2{0,-32}/1024, {0,0,0,0}, {0,1-grid_alpha});
            Draw::World::Quad(fvec3{corners_dist,corners_dist,0}*f_xy, fvec3{3.2,0,0}*f_xy, fvec3{0,3.2,0}*f_xy, fvec2{192,96}/1024, fvec2{-32,0}/1024, fvec2{0,-32}/1024, {0,0,0,0}, {0,1-grid_alpha});
            Draw::World::Quad(fvec3{corners_dist,corners_dist,0}*f_y,  fvec3{3.2,0,0}*f_y,  fvec3{0,3.2,0}*f_y,  fvec2{192,96}/1024, fvec2{-32,0}/1024, fvec2{0,-32}/1024, {0,0,0,0}, {0,1-grid_alpha});

            // Sides
            Draw::World::Quad(fvec3{corners_dist+3.2,-1.65,0}, fvec3{0,3.2,0}, fvec3{-3.2,0,0}, fvec2{160,96}/1024, fvec2{32,0}/1024, fvec2{0,32}/1024, {0,0,0,0}, {0,1-grid_alpha});
            Draw::World::Quad(fvec3{1.65,corners_dist+3.2,0}, fvec3{-3.2,0,0}, fvec3{0,-3.2,0}, fvec2{160,96}/1024, fvec2{32,0}/1024, fvec2{0,32}/1024, {0,0,0,0}, {0,1-grid_alpha});
            Draw::World::Quad(fvec3{-corners_dist-3.2,1.65,0}, fvec3{0,-3.2,0}, fvec3{3.2,0,0}, fvec2{160,96}/1024, fvec2{32,0}/1024, fvec2{0,32}/1024, {0,0,0,0}, {0,1-grid_alpha});
            Draw::World::Quad(fvec3{-1.65,-corners_dist-3.2,0}, fvec3{3.2,0,0}, fvec3{0,3.2,0}, fvec2{160,96}/1024, fvec2{32,0}/1024, fvec2{0,32}/1024, {0,0,0,0}, {0,1-grid_alpha});

            // Center
            Draw::World::Quad({-0.85,-0.85,0}, {1.6,0,0}, {0,1.6,0}, fvec2{112,0}/1024, fvec2{16,0}/1024, fvec2{0,16}/1024, {0,0,0,0}, {0,1-grid_alpha});

            // Hitmap
            if (hitmap)
            {
                fmat4 saved_matrix = Shaders::world_model_mat;

                Shaders::WorldEnableNormals(1);

                for (int y = 0; y < 20; y++)
                {
                    for (int x = 0; x < 20; x++)
                    {
                        fvec3 center = {x-9.5f, -(y-9.5f), 0};
                        switch ((*hitmap)[y][x])
                        {
                          default:
                            if (!draw_only_dead_ships)
                                break;
                            if (health && (*health)[(*hitmap)[y][x]] == 0)
                                break;
                            Shaders::world_model_mat = saved_matrix /mul/ fmat4::translate(center) /mul/ fmat4::rotate_with_normalized_axis({0,1,0}, to_rad(90.0)) /mul/ fmat4::dia({1.f/12,1.f/12,1.f/12});
                            //Shaders::world_model_mat = fmat4::dia({1.f/12,1.f/12,1.f/12}).mul(fmat4::rotate_with_normalized_axis({0,1,0}, to_rad(90.0))).mul(fmat4::translate(center)).mul(saved_matrix);
                            Shaders::SendMatricesToWorldShader();
                            Shaders::WorldColorFactor({0.25,0.25,0.25});
                            Models::box->DrawTriangles();
                            break;
                          case -1:
                            break;
                          case -2:
                            if (draw_dots)
                                Draw::World::Quad({center.x-0.55f,center.y-0.55f,center.z}, {1.1,0,0}, {0,1.1,0}, fvec2{192,55}/1024, fvec2{11,0}/1024, fvec2{0,11}/1024, {0,0,0,0}, {0,0});
                            break;
                        }
                    }
                }

                Shaders::world_model_mat = saved_matrix;
                Shaders::WorldColorFactor();
                Shaders::WorldEnableNormals(0);
                Shaders::SendMatricesToWorldShader();
            }

            // Selection
            if (selection_rect.x != -1)
            {
                fvec3 center = {selection_rect.x-9.5f-0.55f, -(selection_rect.y-9.5f+0.55f), 0.1};
                Draw::World::Quad(center, {1.1,0,0}, {0,1.1,0}, fvec2{192,33}/1024, fvec2{11,0}/1024, fvec2{0,11}/1024, {0,0,0,0}, {0,0});
            }

            Queues::world->Send();
            Queues::world->DrawTriangles();
        }
    }
};





namespace Net
{
    TCPsocket socket;

    SDLNet_SocketSet socket_set;

    bool netplay = 0;
    bool host = 0;



    struct ShotsList
    {
        uint8_t packet_id = 2;
        ivec2 array[20];
    };



    void Close()
    {
        if (socket)
        {
            SDLNet_TCP_DelSocket(socket_set, socket);
            SDLNet_TCP_Close(socket);
            socket = 0;
        }
    }


    bool Host()
    {
        IPaddress ip;
        ip.host = INADDR_NONE;
        ip.port = ((Config::network_port & 0xff) << 8) | ((Config::network_port & 0xff00) >> 8);
        socket = SDLNet_TCP_Open(&ip);
        if (!socket)
            return 0;
        if (SDLNet_TCP_AddSocket(socket_set, socket) == -1)
        {
            SDLNet_TCP_Close(socket);
            socket = 0;
            return 0;
        }

        return 1;
    }

    void Refresh()
    {
        if (!socket_set)
            return;
        SDLNet_CheckSockets(socket_set, 0);
    }

    bool AnyData()
    {
        if (!socket_set)
            return 0;
        return SDLNet_SocketReady(socket);
    }

    bool Read(void *where, int len) // Returns bool on success.
    {
        while (len > 0)
        {
            int status = SDLNet_TCP_Recv(socket, where, len);
            if (status <= 0) // Error
                return 0;
            where = (char *)where + status;
            len -= status;
        }
        return 1;
    }

    bool Accept()
    {
        if (!socket)
            return 0;

        TCPsocket tmp_sock;
        tmp_sock = SDLNet_TCP_Accept(socket);

        if (!tmp_sock)
            return 0;

        if (SDLNet_TCP_AddSocket(socket_set, tmp_sock) == -1)
        {
            SDLNet_TCP_Close(tmp_sock);
            return 0;
        }

        SDLNet_TCP_DelSocket(socket_set, socket);
        SDLNet_TCP_Close(socket);
        socket = tmp_sock;

        return 1;
    }

    bool Connect(const char *ip_string)
    {
        IPaddress ip;
        if(SDLNet_ResolveHost(&ip, ip_string, Config::network_port) != 0)
            return 0;
        socket = SDLNet_TCP_Open(&ip);
        if (!socket)
            return 0;
        if (SDLNet_TCP_AddSocket(socket_set, socket) == -1)
        {
            SDLNet_TCP_Close(socket);
            socket = 0;
            return 0;
        }
        return 1;
    }

    bool ReadData(int &kick_timer, Board *board, bool *got_board, std::vector<ivec2> *shots, bool *got_shots)
    {
        static constexpr int kick_timeout = 300;
        Refresh();
        if (AnyData())
        {
            kick_timer = 0;

            uint8_t id;
            if (!Read(&id, 1))
            {
                return 0;
            }
            switch (id)
            {
              case 0: // Ping packet
                break;
              case 1: // Board
                if (board)
                    return *got_board = Read(1+(char *)board, (sizeof *board) - 1);
                else
                {
                    Board tmp;
                    return Read(1+(char *)&tmp, (sizeof *board) - 1);
                }
                break;
              case 2: // Shots
                {
                    ShotsList buf;
                    bool status = Read(1+(char *)&buf, (sizeof buf) - 1);
                    if (status == 0)
                        return 0;

                    if (shots)
                    {
                        *got_shots = 1;
                        shots->clear();
                        for (int i = 0; i < 20; i++)
                        {
                            if (buf.array[i].x == -1)
                                break;
                            shots->push_back(buf.array[i]);
                        }
                    }
                    return 1;
                }
                break;
            }
        }
        else
        {
            kick_timer++;
            if (kick_timer >= kick_timeout)
                return 0;
        }
        return 1;
    }

    bool Send(const void *what, int len)
    {
        int status = SDLNet_TCP_Send(socket, what, len);
        return status == len;
    }

    bool SendPing()
    {
        if (!Sys::NewSecond())
            return 1;

        uint8_t val = 0;
        return Send(&val, 1);
    }

    bool SendBoard(const Board &board)
    {
        return Send(&board, sizeof board);
    }

    bool SendShots(const std::vector<ivec2> &list)
    {
        ShotsList buf;

        for (auto &it : buf.array)
            it = {-1,-1};

        auto it = list.begin();
        for (int i = 0; i < 20; i++)
        {
            if (it == list.end())
                break;
            buf.array[i] = *it;
            it++;
        }

        return Send(&buf, sizeof buf);
    }
}






void Resize()
{
    fmat4 m;

    Shaders::gui->Use();
    static constexpr ivec2 ref_gui_size{320,240};
    float gui_scale = 0;
    if (float(Window::Size().x) / float(Window::Size().y) > float(ref_gui_size.x) / float(ref_gui_size.y))
    {
        Info::gui_texture_size.y = ref_gui_size.y;
        Info::gui_texture_size.x = ((int)std::round(ref_gui_size.y * float(Window::Size().x) / float(Window::Size().y)) + 1) / 2 * 2;
        gui_scale = float(Window::Size().y) / float(ref_gui_size.y);
    }
    else
    {
        Info::gui_texture_size.x = ref_gui_size.x;
        Info::gui_texture_size.y = ((int)std::round(ref_gui_size.x / float(Window::Size().x) * float(Window::Size().y)) + 1) / 2 * 2;
        gui_scale = float(Window::Size().x) / float(ref_gui_size.x);
    }

    Shaders::gui->SetUniform<float>(3, gui_scale > 4 ? gui_scale : 4);
    Shaders::gui->SetUniform<fvec2>(1, fvec2(Info::gui_texture_size));
    //glUniform1f(Shaders::gui->GetUniformLocation(3), gui_scale > 4 ? gui_scale : 4);
    //glUniform2fv(Shaders::gui->GetUniformLocation(1), 1, fvec2(Info::gui_texture_size).as_array());


    Shaders::flat->Use();
    m = fmat4::ortho(Info::gui_texture_size * ivec2{1,-1},-1,1);
    Shaders::flat->SetUniform<fmat4>(2, m);
    //glUniformMatrix4fv(Shaders::flat->GetUniformLocation(2), 1, 0, m.as_array());

    Framebuffers::gui_texture->SetData(Info::gui_texture_size);

    Shaders::world->Use();
    Shaders::world_proj_mat = fmat4::perspective(to_rad(60.0), float(Window::Size().x) / float(Window::Size().y), 1, 100);
    Shaders::SendMatricesToWorldShader();
}

void Boot()
{
    MarkLocation("Boot");
    Sys::SetCurrentFunction(Menu);
    //Sys::SetCurrentFunction(Game::ShipPlacement);
    //Sys::SetCurrentFunction(Game::World);

    { // Configuring
        MarkLocation("Configuring");

        Input::ShowMouseImmediate(0);

        Graphics::Blend::Enable();
        Graphics::Blend::Presets::FuncNormal_Pre();

        Info::rng.seed(Utils::Clock::Time());


        // Selecting locale
        bool need_english;
        FILE *fptr = std::fopen("locale", "r");
        if (fptr)
        {
            need_english = std::getc(fptr) == '0';
            std::fclose(fptr);
        }
        else
        {
            need_english = 1;
            fptr = std::fopen("locale", "w");
            if (fptr)
            {
                std::putc('0', fptr);
                std::fclose(fptr);
            }
        }
        if (need_english != Locale::English())
            Locale::Switch();
    }

    { // Loading models
        MarkLocation("Models");
        void LoadModels();
        LoadModels();
    }

    { // Loading textures
        MarkLocation("Loading textures");
        Graphics::ImageData img;
        #define TEXTURE(x,y,z) if (Config::tex_load_compressed_instead_of_tga) \
                                   img.LoadCompressed("assets/" #x); \
                               else \
                                   img.LoadTGA("assets/" #x ".tga"); \
                               if (Config::tex_save_tga) \
                                   img.SaveTGA("assets/" #x ".tga"); \
                               if (Config::tex_save_compressed) \
                                   img.SaveCompressed("assets/" #x); \
                               Textures::x = new Graphics::Texture(img); \
                               Textures::x->LinearInterpolation(y); \
                               Textures::x->WrapModeXY(Graphics::WrapMode::z);
        TEXTURES_LIST
        #undef TEXTURE
    }

    { // Loading sounds
        MarkLocation("Loading sounds");
        Audio::SoundData data;
        #define SOUND(name) data.LoadWAV("assets/" #name ".wav"); \
                            Sounds::name = new Audio::Buffer(data);
        SOUNDS_LIST
        #undef SOUND
    }

    { // Creating framebuffers
        MarkLocation("Framebuffers");

        Framebuffers::gui_texture = new Graphics::Texture;
        Framebuffers::gui_texture->LinearInterpolation(0);
        Framebuffers::gui_texture->WrapModeXY(Graphics::WrapMode::clamp);
        Framebuffers::gui = new Graphics::Framebuffer;
        Framebuffers::gui->BindDrawAndAttachTexture(*Framebuffers::gui_texture);

        Graphics::Framebuffer::UnbindDraw();
    }

    { // Setting up shaders
        MarkLocation("Shaders");

        Shaders::flat = new Graphics::Shader("Flat", ShaderSources::flat, {"a_pos","a_texpos","a_color","a_fac"}, {"u_texture","u_texture_size","u_matrix"});
        Shaders::flat->Use();
        Shaders::flat->SetUniform<Graphics::Texture>(0, *Textures::gui);
        //Textures::gui->SetUniform(Shaders::flat->GetUniformLocation(0));
        Shaders::flat->SetUniform<fvec2>(1, {1024,1024});
        //glUniform2f(Shaders::flat->GetUniformLocation(1), 1024, 1024);

        Shaders::gui = new Graphics::Shader("GUI", ShaderSources::gui, {"a_pos"}, {"u_texture","u_texture_size","u_mouse_pos","u_scale","u_mouse_exists"});
        Shaders::gui->Use();
        Shaders::gui->SetUniform<Graphics::Texture>(0, *Framebuffers::gui_texture);
        //Framebuffers::gui_texture->SetUniform(Shaders::gui->GetUniformLocation(0));

        Shaders::world = new Graphics::Shader("World", ShaderSources::world, {"a_pos","a_normal","a_texpos","a_color","a_fac","a_tex"}, {"u_matrix","u_model_matrix","u_textures","u_cam_pos","u_enable_normals","u_color_factor"});
        Shaders::world->Use();
        Shaders::WorldColorFactor();
        Shaders::WorldEnableNormals(0);

        Shaders::world->SetUniform<Graphics::Texture>(2, *Textures::gui,           0);
        Shaders::world->SetUniform<Graphics::Texture>(2, *Textures::skybox_front,  1);
        Shaders::world->SetUniform<Graphics::Texture>(2, *Textures::skybox_back,   2);
        Shaders::world->SetUniform<Graphics::Texture>(2, *Textures::skybox_left,   3);
        Shaders::world->SetUniform<Graphics::Texture>(2, *Textures::skybox_right,  4);
        Shaders::world->SetUniform<Graphics::Texture>(2, *Textures::skybox_top,    5);
        Shaders::world->SetUniform<Graphics::Texture>(2, *Textures::skybox_bottom, 6);
        //Textures::gui          ->SetUniform(Shaders::world->GetUniformLocation(2)+0);
        //Textures::skybox_front ->SetUniform(Shaders::world->GetUniformLocation(2)+1);
        //Textures::skybox_back  ->SetUniform(Shaders::world->GetUniformLocation(2)+2);
        //Textures::skybox_left  ->SetUniform(Shaders::world->GetUniformLocation(2)+3);
        //Textures::skybox_right ->SetUniform(Shaders::world->GetUniformLocation(2)+4);
        //Textures::skybox_top   ->SetUniform(Shaders::world->GetUniformLocation(2)+5);
        //Textures::skybox_bottom->SetUniform(Shaders::world->GetUniformLocation(2)+6);
    }

    { // Setting up models
        MarkLocation("Models");
        Models::GUI::quad = new Graphics::VertexArray<Layouts::GUI>({{{0,0}},{{1,0}},{{0,1}},{{0,1}},{{1,0}},{{1,1}}});
    }

    { // Creating rendering queues
        MarkLocation("Queues");
        Queues::gui = new Graphics::RenderingQueue<Layouts::Flat>("GUI", 0x0C00);
        Queues::world = new Graphics::RenderingQueue<Layouts::World>("World", 0x0C00);
    }

    { // Initializing network
        MarkLocation("Network");
        Net::socket_set = SDLNet_AllocSocketSet(2);
        if (Net::socket_set == 0)
            Sys::Error("Can't create a socket set.");
    }

    { // Selecting locale

    }

}




void Menu()
{
    MarkLocation("Menu");

    Input::RelativeMouseMode(0);

    int button_states[4]{};
    int current_button = -1;

    int fade = 0;
    bool exiting = 0;

    static constexpr int menu_y_pos = 10;
    static constexpr int button_hor_collider = 192;
    static constexpr int button_y_shift = 25;

    static constexpr ivec2 logo_tex_size{300,120};

    auto GUI_Tick = [&]
    {
        if (exiting)
        {
            fade--;
            return;
        }
        else if (fade != Config::fade_upper_cap)
        {
            fade++;
        }

        current_button = -1;
        int pos = 0;
        for (auto &it : button_states)
        {
            if (Info::GuiMouseInRect({-button_hor_collider/2,menu_y_pos-3+pos*button_y_shift},{button_hor_collider,24}))
            {
                current_button = pos;
                it++;
                if (it > Config::menu_button_timer_upper_cap)
                    it = Config::menu_button_timer_upper_cap;
            }
            else
            {
                it--;
                if (it < 0)
                    it = 0;
            }
            pos++;
        }

        if (Input::MouseButtonPressed(1) && current_button != -1)
        {
            Sounds::menu_click->Play();

            switch (current_button)
            {
              case 0:
                Net::netplay = 0;
                Net::host = 0;
                Sys::SetCurrentFunction(Game::ShipPlacement);
                break;
              case 1:
                Net::netplay = 1;
                Net::host = 1;
                Sys::SetCurrentFunction(Game::NetConfig);
                break;
              case 2:
                Net::netplay = 1;
                Net::host = 0;
                Sys::SetCurrentFunction(Game::NetConfig);
                break;
              case 3:
                Sys::SetCurrentFunction(0);
                break;
            }

            exiting = 1;
        }
    };

    auto GUI_Render = [&]
    {
        int pos = 0;
        for (const char *it : {Locale::menu_0(), Locale::menu_1(), Locale::menu_2(), Locale::menu_3()})
        {
            int item_y = menu_y_pos+pos*button_y_shift;
            float cur_button_state = button_states[pos] / float(Config::menu_button_timer_upper_cap);

            bool pressed = Input::MouseButtonDown(1) && current_button == pos;

            Draw::GUI::TextLargeC(it, ivec2{0,item_y+pressed}+1, Config::menu_text_shadow_color.interpolate(Config::menu_text_selected_shadow_color, cur_button_state)); // Fixed
            Draw::GUI::TextLargeC(it, {0,item_y+pressed}, Config::menu_text_color.interpolate(Config::menu_text_selected_color, cur_button_state)); // Fixed

            pos++;
        }



        // Logo
        Draw::GUI::Rect(-logo_tex_size/2 - ivec2{0,64}, logo_tex_size, {272,0});

        // Developer
        Draw::GUI::Rect({-192/2, Info::gui_texture_size.y/2-16}, {192,12}, {0,500});



        Draw::GUI::LocaleSwitch();

        Draw::GUI::Cursor();

        if (fade != Config::fade_upper_cap)
        {
            Draw::GUI::RectFill(-Info::gui_texture_size/2, Info::gui_texture_size, {0,0,0,1 - float(fade)/Config::fade_upper_cap});
        }
    };

    Graphics::Depth(0);

    Utils::TickStabilizer ts(60,8);

    while (1)
    {
        Sys::BeginFrame();

        while (ts.Tick())
        {
            Sys::Tick();
            GUI_Tick();
        }

        Shaders::flat->Use();
        Framebuffers::gui->BindDrawAndClear();
        Graphics::ViewportObj(*Framebuffers::gui_texture);
        Queues::gui->Clear();

        GUI_Render();

        Queues::gui->Send();
        Queues::gui->DrawTriangles();

        Shaders::gui->Use();
        Shaders::SendMousePosToGuiShader();

        Graphics::Framebuffer::UnbindDraw();
        Graphics::ViewportFullscreen2y();
        Models::GUI::quad->DrawTriangles(0,6);

        if (exiting && fade <= 0)
            return;

        Sys::EndFrame();
    }
}

namespace Game
{
    Board player_board;
    Board enemy_board;

    void ShipPlacement()
    {
        MarkLocation("Ship placement");

        Input::RelativeMouseMode(0);

        player_board.Reset();
        enemy_board.Reset();

        int fade = 0;
        bool exiting = 0;

        static constexpr int hor_ships_offset = 115;
        static constexpr int ship_box_size = 74;
        static constexpr int ship_list_y_margin = 32;
        static constexpr int ship_frame_fade_cap = 20;
        static constexpr int unused_ship_alpha_fade_cap = 30;
        static constexpr int start_game_button_fade_cap = 30;
        static constexpr int start_game_button_y_margin = 6;
        static constexpr float unused_ship_max_alpha_dimming = 0.7;

        enum class State
        {
            nothing,
            change_me,
            ship_selected,
            waiting_for_data,
        };

        State state = State::nothing;

        State next_state = State::nothing;

        int ship_list_size_y = Info::gui_texture_size.y - ship_list_y_margin;
        ivec2 first_ship_center{hor_ships_offset, (int)std::round(-ship_list_size_y/2 + ship_box_size/2 - float(ship_list_size_y/2) * (ship_box_size * 5 - ship_list_size_y) / ship_list_size_y)};

        bool mouse_on_ship_list = 0;
        int selected_ship = -1;

        int ship_frame_colors[5]{};
        int ship_icons_alpha[5];
        for (auto &it : ship_icons_alpha)
            it = unused_ship_alpha_fade_cap;

        int non_selected_ships_alpha = unused_ship_alpha_fade_cap;

        int saved_ship_rotations[5]{};

        bool available_ships[5]{1,1,1,1,1};

        int ref_first_ship_center_y = first_ship_center.y;

        bool draw_start_game_button = 0;
        int start_game_button_alpha = 0;

        int start_game_button_color = 0;
        bool start_game_button_hovered = 0;



        int net_kick_timer = 0;
        bool net_got_enemy_board = 0;


        auto GUI_Tick = [&]
        {
            if (exiting)
            {
                fade--;
                return;
            }
            else if (fade != Config::fade_upper_cap)
            {
                fade++;
            }


            // Back to the menu if Escape is pressed
            if (Input::KeyPressed(Input::Key_Esc()))
            {
                exiting = 1;
                Sys::SetCurrentFunction(Menu);
                Net::Close();
                Sounds::open_menu->Play();
            }


            mouse_on_ship_list = Info::GuiMousePos().x >= hor_ships_offset - ship_box_size/2 && Info::GuiMousePos().x < hor_ships_offset + ship_box_size/2;

            if (state == State::nothing)
            {
                if (!mouse_on_ship_list)
                {
                    selected_ship = -1;

                    // Removing a ship
                    if (Input::MouseButtonPressed(1))
                    {
                        int ship_under_cursor = player_board.At(Info::GuiBoardMousePos());
                        if (ship_under_cursor > 1)
                        {
                            Sounds::ship_rotation->Play();
                            selected_ship = 6 - ship_under_cursor;
                            available_ships[selected_ship] = 1;
                            state = State::change_me;
                            next_state = State::ship_selected;
                            player_board.DeleteShipAt(Info::GuiBoardMousePos());
                        }
                    }
                }
                else
                {
                    // Selecting ship in the list
                    selected_ship = -1;
                    for (int i = 0; i < 5; i++)
                    {
                        ivec2 center{first_ship_center.x, first_ship_center.y + i * ship_box_size};

                        if (available_ships[i] && Info::GuiMouseInRect(center-ship_box_size/2, {ship_box_size, ship_box_size}))
                        {
                            selected_ship = i;
                            break;
                        }
                    }

                    // Ship list clicked
                    if (Input::MouseButtonPressed(1) && selected_ship != -1)
                    {
                        Sounds::menu_click->Play();
                        state = State::change_me;
                        next_state = State::ship_selected;
                    }
                }
            }

            // Moving ship list
            if (!draw_start_game_button)
            {
                int target_y;
                if (mouse_on_ship_list)
                {
                    ship_list_size_y = Info::gui_texture_size.y - ship_list_y_margin;
                    ref_first_ship_center_y = std::round(-ship_list_size_y/2 + ship_box_size/2 - float(Info::GuiMousePos().y + ship_list_size_y/2) * (ship_box_size * 5 - ship_list_size_y) / ship_list_size_y);
                }


                if (state == State::ship_selected)
                    target_y = -selected_ship*ship_box_size;
                else
                    target_y = ref_first_ship_center_y;
                int diff = target_y - first_ship_center.y;
                if (diff != 0)
                    first_ship_center.y += std::round(std::sqrt(std::abs(diff)) * sign(diff));
            }

            // Managing non-selected  ships' alpha
            if (state == State::ship_selected)
            {
                if (non_selected_ships_alpha != 0)
                    non_selected_ships_alpha--;
            }
            else
            {
                if (non_selected_ships_alpha != unused_ship_alpha_fade_cap)
                    non_selected_ships_alpha++;
            }

            // Managing used ships' alpha
            for (int i = 0; i < 5; i++)
            {
                if (available_ships[i])
                {
                    if (ship_icons_alpha[i] != unused_ship_alpha_fade_cap)
                        ship_icons_alpha[i]++;
                }
                else
                {
                    if (ship_icons_alpha[i] != 0)
                        ship_icons_alpha[i]--;
                }
            }

            // Coloring ships' frames
            for (int i = 0; i < 5; i++)
            {
                if (selected_ship == i)
                {
                    if (ship_frame_colors[i] != ship_frame_fade_cap)
                        ship_frame_colors[i]++;
                }
                else
                {
                    if (ship_frame_colors[i] != 0)
                        ship_frame_colors[i]--;
                }
            }

            // Placing a ship
            if (state == State::ship_selected)
            {
                // Rotation
                if (Input::MouseButtonPressed(3))
                {
                    Sounds::ship_rotation->Play();
                    saved_ship_rotations[selected_ship] = (saved_ship_rotations[selected_ship] + 1) % Config::ship_tile_models[selected_ship].Size();
                }

                // Placement
                if (Input::MouseButtonPressed(1))
                {
                    if (Info::GuiBoardMousePos().x != -1)
                    {
                        if (player_board.CanPlaceShipAt(selected_ship, Info::GuiBoardMousePos(), saved_ship_rotations[selected_ship]))
                        {
                            Sounds::menu_click->Play();

                            player_board.PlaceShipAt(selected_ship, Info::GuiBoardMousePos(), saved_ship_rotations[selected_ship]);

                            available_ships[selected_ship] = 0;
                            state = State::nothing;
                        }
                        else
                        {
                            Sounds::menu_error->Play();
                        }
                    }
                    else
                    {
                        Sounds::menu_click->Play();
                        state = State::nothing;
                    }
                }
            }


            // Deciding to draw or not to draw `Start game` button
            draw_start_game_button = 1;
            for (int it : available_ships)
            {
                if (it)
                {
                    draw_start_game_button = 0;
                    break;
                }
            }

            // Managing `Start game` button alpha
            if (draw_start_game_button && state != State::waiting_for_data)
            {
                if (start_game_button_alpha != start_game_button_fade_cap)
                    start_game_button_alpha++;
            }
            else
            {
                if (start_game_button_alpha != 0)
                    start_game_button_alpha--;
            }

            // Managing `Start game` button color and clicks
            if (draw_start_game_button && Info::GuiMouseInRect({hor_ships_offset-ship_box_size/2, -18-start_game_button_y_margin}, {ship_box_size, 36+start_game_button_y_margin*2}))
            {
                start_game_button_hovered = 1;
                if (start_game_button_color != Config::menu_button_timer_upper_cap)
                    start_game_button_color++;

                if (Input::MouseButtonPressed(1) && start_game_button_alpha == start_game_button_fade_cap && state != State::waiting_for_data)
                {
                    Sounds::menu_click->Play();
                    if (Net::netplay == 0)
                    {
                        exiting = 1;
                        Sys::SetCurrentFunction(Game::World);
                    }
                    else
                    {
                        state = State::waiting_for_data;
                        if (Net::SendBoard(player_board) == 0)
                        {
                            Sys::SetCurrentFunction(Menu);
                            exiting = 1;
                        }
                    }
                }
            }
            else
            {
                start_game_button_hovered = 0;
                if (start_game_button_color != 0)
                    start_game_button_color--;
            }



            // Network
            if (Net::netplay)
            {
                // Send ping
                if (Net::SendPing() == 0)
                {
                    Sys::SetCurrentFunction(Menu);
                    exiting = 1;
                }



                // Receive
                if (Net::ReadData(net_kick_timer, &enemy_board, &net_got_enemy_board, 0, 0) == 0)
                {
                    Sys::SetCurrentFunction(Menu);
                    exiting = 1;
                }

                if (state == State::waiting_for_data && net_got_enemy_board)
                {
                    net_got_enemy_board = 0;
                    Sys::SetCurrentFunction(World);
                    exiting = 1;
                }
            }



            if (state == State::change_me)
                state = next_state;
        };

        auto GUI_Render = [&]
        {
            // Your code here.

            // Drawing grid
            Draw::GUI::ShipGrid();
            player_board.Render();

            // Drawing ship preview
            if (state == State::ship_selected && Info::GuiBoardMousePos().x != -1)
            {
                for (const ivec2 &offset : Config::ship_tile_models[selected_ship][saved_ship_rotations[selected_ship]])
                {
                    ivec2 tile_pos = Info::GuiBoardMousePos()+offset;

                    if (player_board.At(tile_pos) == 0)
                        Draw::GUI::ShipGridSquare(tile_pos, {1,0.5,0,1}, 0);
                    else
                        Draw::GUI::ShipGridSquare(tile_pos, {0.4,0,0,1}, 2);
                }
            }

            if (non_selected_ships_alpha != unused_ship_alpha_fade_cap)
                Draw::GUI::TextC(Locale::ship_rotation_hint(), {Config::board_hor_offset,106}, {1,0.5,0,1 - non_selected_ships_alpha / float(unused_ship_alpha_fade_cap)});

            /* // Debug map editor
            if (Input::MouseButtonDown(1))
                player_board.At(Info::GuiBoardMousePos()) = 2;
            if (Input::MouseButtonDown(2))
                player_board.At(Info::GuiBoardMousePos()) = 1;
            if (Input::MouseButtonDown(3))
                player_board.At(Info::GuiBoardMousePos()) = 0;
            */

            // Drawing ship list
            for (int i = 0; i < 5; i++)
            {
                // Icon
                float icon_alpha = 1;
                if (ship_icons_alpha[i] != unused_ship_alpha_fade_cap)
                    icon_alpha = ship_icons_alpha[i] / float(unused_ship_alpha_fade_cap);
                if (state == State::ship_selected && i != selected_ship)
                    icon_alpha = std::min(icon_alpha, non_selected_ships_alpha / float(unused_ship_alpha_fade_cap));
                Draw::GUI::Rect(ivec2{first_ship_center.x, first_ship_center.y + i*ship_box_size}-32, {64,64}, {208,256-i*64}, {64,64}, {0,0,0,0}, {0,(1-icon_alpha)*unused_ship_max_alpha_dimming});


                // Frame
                fvec4 frame_color;
                if (i == selected_ship && state == State::ship_selected)
                    frame_color = {1,0.5,0,1};
                else
                    frame_color = fvec4(0.2,0.2,0.2,1).interpolate(fvec4(1), ship_frame_colors[i] / float(ship_frame_fade_cap)); // Fixed

                Draw::GUI::RectFrame(ivec2{first_ship_center.x, first_ship_center.y + i*ship_box_size}-34, {68,68}, frame_color);
            }


            // Drawing `Start game` button
            if (start_game_button_alpha)
            {
                static constexpr int alpha_reduction_stages = 8;

                for (int i = 0; i < alpha_reduction_stages; i++)
                {
                    Draw::GUI::RectFill({hor_ships_offset-ship_box_size/2, -18-start_game_button_y_margin-i}, {ship_box_size, 36+start_game_button_y_margin*2+i*2}, {0,0,0,start_game_button_alpha / float(start_game_button_fade_cap) - i / float(alpha_reduction_stages)});
                }

                bool pressed = Input::MouseButtonDown(1) && start_game_button_hovered && start_game_button_alpha == start_game_button_fade_cap && state != State::waiting_for_data;

                Draw::GUI::TextLargeC(Locale::start_game(), {hor_ships_offset+1,-17+pressed}, Config::menu_text_shadow_color.interpolate(Config::menu_text_selected_shadow_color,start_game_button_color/float(Config::menu_button_timer_upper_cap)) * fvec4{1,1,1,start_game_button_alpha / float(start_game_button_fade_cap)}); // Fixed
                Draw::GUI::TextLargeC(Locale::start_game(), {hor_ships_offset,-18+pressed}, Config::menu_text_color.interpolate(Config::menu_text_selected_color,start_game_button_color/float(Config::menu_button_timer_upper_cap)) * fvec4{1,1,1,start_game_button_alpha / float(start_game_button_fade_cap)}); // Fixed
            }


            // Drawing `Waiting for opponent` text
            if (state == State::waiting_for_data)
            {
                Draw::GUI::RectFill({-Info::gui_texture_size.x/2, -32}, {Info::gui_texture_size.x, 64}, {0,0,0,0.6f * (1 - start_game_button_alpha / float(start_game_button_fade_cap))});
                Draw::GUI::RectFill({-Info::gui_texture_size.x/2, -20}, {Info::gui_texture_size.x, 40}, {0,0,0,1 - start_game_button_alpha / float(start_game_button_fade_cap)});
                Draw::GUI::TextC(Locale::waiting_for_enemy_board(), {0,-6}, {1,1,1,1 - start_game_button_alpha / float(start_game_button_fade_cap)});
            }


            Draw::GUI::Cursor();


            // Screen dimming code
            if (fade != Config::fade_upper_cap)
            {
                Draw::GUI::RectFill(-Info::gui_texture_size/2, Info::gui_texture_size, {0,0,0,1 - float(fade)/Config::fade_upper_cap});
            }
        };


        Graphics::Depth(0);

        Utils::TickStabilizer ts(60,8);

        while (1)
        {
            Sys::BeginFrame();

            while (ts.Tick())
            {
                Sys::Tick();
                GUI_Tick();
            }

            Shaders::flat->Use();
            Framebuffers::gui->BindDrawAndClear();
            Graphics::ViewportObj(*Framebuffers::gui_texture);
            Queues::gui->Clear();

            GUI_Render();

            Queues::gui->Send();
            Queues::gui->DrawTriangles();

            Shaders::gui->Use();
            Shaders::SendMousePosToGuiShader();

            Graphics::Framebuffer::UnbindDraw();
            Graphics::ViewportFullscreen2y();
            Models::GUI::quad->DrawTriangles(0,6);

            if (exiting && fade <= 0)
            {
                if (Sys::CurrentFunction() != World)
                    Net::Close();
                return;
            }

            Sys::EndFrame();
        }
    }

    void NetConfig()
    {
        MarkLocation("Network config");

        Input::RelativeMouseMode(0);

        int fade = 0;
        bool exiting = 0;

        static constexpr int error_msg_timer_cap = 90;



        std::string error_msg;
        int error_timer = 0;


        static std::string ip_string;




        if (Net::host)
        {
            if (!Net::Host())
                error_msg = Jo(Locale::cant_create_server(), Config::network_port, '.');
        }





        auto GUI_Tick = [&]
        {
            if (exiting)
            {
                fade--;
                return;
            }
            else if (fade != Config::fade_upper_cap)
            {
                fade++;
            }


            if (error_msg.size() != 0)
            {
                if (error_timer != error_msg_timer_cap)
                {
                    error_timer++;
                    return;
                }
                else
                {
                    Sys::SetCurrentFunction(Menu);
                    exiting = 1;
                }
            }
            else
            {
                if (Input::KeyPressed(Input::Key_Esc()))
                {
                    Sys::SetCurrentFunction(Menu);
                    Sounds::open_menu->Play();
                    Net::Close();
                    exiting = 1;
                }

                if (Net::host)
                {
                    if (Net::Accept())
                    {
                        Sys::SetCurrentFunction(ShipPlacement);
                        exiting = 1;
                    }
                }
                else
                {
                    if (ip_string.size() < 15)
                    {
                        if (Input::KeyPressed(Input::Key<'.'>()) ||
                            Input::KeyPressed(Input::Key<','>()) ||
                            Input::KeyPressed(Input::Key<'/'>()) ||
                            Input::KeyPressed(Input::Key_Num<'.'>()))
                        {
                            ip_string += '.';
                            Sounds::key->Play();
                        }
                        if (Input::KeyPressed(Input::Key<'0'>()) || Input::KeyPressed(Input::Key_Num<'0'>())) {ip_string += '0'; Sounds::key->Play();}
                        if (Input::KeyPressed(Input::Key<'1'>()) || Input::KeyPressed(Input::Key_Num<'1'>())) {ip_string += '1'; Sounds::key->Play();}
                        if (Input::KeyPressed(Input::Key<'2'>()) || Input::KeyPressed(Input::Key_Num<'2'>())) {ip_string += '2'; Sounds::key->Play();}
                        if (Input::KeyPressed(Input::Key<'3'>()) || Input::KeyPressed(Input::Key_Num<'3'>())) {ip_string += '3'; Sounds::key->Play();}
                        if (Input::KeyPressed(Input::Key<'4'>()) || Input::KeyPressed(Input::Key_Num<'4'>())) {ip_string += '4'; Sounds::key->Play();}
                        if (Input::KeyPressed(Input::Key<'5'>()) || Input::KeyPressed(Input::Key_Num<'5'>())) {ip_string += '5'; Sounds::key->Play();}
                        if (Input::KeyPressed(Input::Key<'6'>()) || Input::KeyPressed(Input::Key_Num<'6'>())) {ip_string += '6'; Sounds::key->Play();}
                        if (Input::KeyPressed(Input::Key<'7'>()) || Input::KeyPressed(Input::Key_Num<'7'>())) {ip_string += '7'; Sounds::key->Play();}
                        if (Input::KeyPressed(Input::Key<'8'>()) || Input::KeyPressed(Input::Key_Num<'8'>())) {ip_string += '8'; Sounds::key->Play();}
                        if (Input::KeyPressed(Input::Key<'9'>()) || Input::KeyPressed(Input::Key_Num<'9'>())) {ip_string += '9'; Sounds::key->Play();}
                    }
                    if (ip_string.size() > 0 && Input::KeyPressed(Input::Key_Backspace()))
                    {
                        ip_string.resize(ip_string.size()-1);
                        Sounds::key->Play();
                    }

                    if (Input::KeyPressed(Input::Key<'V'>()) && (Input::KeyDown(Input::Key_Ctrl_L()) || Input::KeyDown(Input::Key_Ctrl_R())))
                    {
                        const char *tmp = SDL_GetClipboardText();
                        while (ip_string.size() < 15)
                        {
                            if (*tmp != '.' && (*tmp < '0' || *tmp > '9'))
                                break;
                            ip_string += *tmp;
                            tmp++;
                        }
                    }

                    if (Input::KeyPressed(Input::Key_Enter()) || Input::KeyPressed(Input::Key_Num_Enter()))
                    {
                        if (Net::Connect(ip_string.c_str()))
                        {
                            Sounds::menu_click->Play();
                            Sys::SetCurrentFunction(ShipPlacement);
                            exiting = 1;
                        }
                        else
                        {
                            Sounds::menu_error->Play();
                        }
                    }
                }
            }
        };

        auto GUI_Render = [&]
        {
            if (error_msg.size() != 0)
            {
                Draw::GUI::TextC(error_msg.c_str(), {0,0});
            }
            else
            {
                if (Net::host)
                {
                    Draw::GUI::TextLargeC(Locale::waiting_for_client(), {0,-9});
                    Draw::GUI::TextC(Jo(Locale::port(), Config::network_port), {0, 16});
                }
                else
                {
                    Draw::GUI::TextLargeC(Locale::ip(), {0,-20});
                    Draw::GUI::TextLargeC(ip_string.c_str(), {0,0});
                    if (Utils::Clock::Time() % Utils::Clock::Tps() < Utils::Clock::Tps()/2)
                        Draw::GUI::LineV({int(ip_string.size() * 13 / 2+1), 0}, 18, {1,1,1,1});
                }
            }


            Draw::GUI::Cursor();

            if (fade != Config::fade_upper_cap)
            {
                Draw::GUI::RectFill(-Info::gui_texture_size/2, Info::gui_texture_size, {0,0,0,1 - float(fade)/Config::fade_upper_cap});
            }
        };


        SDL_StartTextInput();

        Graphics::Depth(0);

        Utils::TickStabilizer ts(60,8);

        while (1)
        {
            Sys::BeginFrame();

            while (ts.Tick())
            {
                Sys::Tick();
                GUI_Tick();
            }

            Shaders::flat->Use();
            Framebuffers::gui->BindDrawAndClear();
            Graphics::ViewportObj(*Framebuffers::gui_texture);
            Queues::gui->Clear();

            GUI_Render();

            Queues::gui->Send();
            Queues::gui->DrawTriangles();

            Shaders::gui->Use();
            Shaders::SendMousePosToGuiShader();

            Graphics::Framebuffer::UnbindDraw();
            Graphics::ViewportFullscreen2y();
            Models::GUI::quad->DrawTriangles(0,6);

            if (exiting && fade <= 0)
            {
                if (Sys::CurrentFunction() != ShipPlacement)
                    Net::Close();

                SDL_StopTextInput();
                return;
            }

            Sys::EndFrame();
        }
    }

    void World()
    {
        MarkLocation("World");

        //"Debug"; player_board.RandomizeShips();

        if (Net::netplay == 0)
            enemy_board.RandomizeShips();

        Input::RelativeMouseMode(1);

        Shaders::world_cam_pos = {0,0,0};

        int fade = 0;
        bool exiting = 0;


        static constexpr float boards_displacement = 16;
        static constexpr int turn_end_time_cap = 160, turn_end_bar_end_x = 48;
        static constexpr int delay_before_first_bullet = 90, delay_between_bullets = 25;
        static constexpr float bullet_distance_per_tick = 0.05;
        static constexpr int fire_prob_divisor = 3;
        static constexpr float fire_size_divisor = 2.3;

        static constexpr int game_over_msg_full_alpha_time = 120;
        static constexpr int game_over_timer_cap = 180;
        static constexpr int game_over_msg_background_end_x = 100;
        static constexpr int game_over_msg_y_displacement = 40;
        static constexpr int game_over_msg_text_offset = 1;
        static constexpr int game_over_press_any_key_displacement = 35;
        static constexpr bool bot_always_hits_perfectly = 0;
        static constexpr int first_turn_hint_offset = 32;

        static constexpr ivec2 menu_rect_sz{150,60};
        static constexpr int menu_rect_margin = 12;
        static constexpr int menu_y_offset = 3;
        static constexpr int menu_button_collider_w = 68;


        enum class State
        {
            selecting_cells,
            watching_battle,
            finished,
            waiting_for_data,
        };

        State state = State::selecting_cells;


        float camera_distance = 0;

        int turn_end_timer = 0;

        int remaining_shots = 20;

        int enemy_remaining_shots = 20;


        std::vector<ivec2> shots_list;

        struct Bullet
        {
            fvec3 src, dst, pos;
            float dist;
            fmat3 rotation;
            bool hit;
            ivec2 target;
        };
        std::vector<Bullet> bullets;
        int time_to_next_bullet = 0;
        int what_ship_shoots_next = 0;
        int shot_ship_counter = 0;

        int enemy_hit_map[20][20]; // -1 = nothing, -2 = dot, 0..4 = ship id
        for (auto &arr : enemy_hit_map)
            for (auto &it : arr)
                it = -1;


        std::vector<ivec2> other_shots_list;
        std::vector<Bullet> other_bullets;
        int other_time_to_next_bullet = 0;
        int other_what_ship_shoots_next = 0;
        int other_shot_ship_counter = 0;

        int other_enemy_hit_map[20][20]; // -1 = nothing, -2 = dot, 0..4 = ship id
        for (auto &arr : other_enemy_hit_map)
            for (auto &it : arr)
                it = -1;



        int player_ship_hp[5]{6,5,4,3,2};
        int enemy_ship_hp[5]{6,5,4,3,2};


        bool player_won = 0;
        int game_over_timer = 0;


        bool menu_open = 0;
        bool menu_show = 0;
        int menu_buttons_alpha[2]{};
        int menu_selected_button = -1;


        bool first_turn_hint = 1;



        static constexpr float expl_grow_speed = 0.1, expl_destr_speed = 0.05;

        struct Explosion
        {
            fvec3 pos;
            float target_size;
            float state;
            bool decr;
        };
        std::vector<Explosion> explosions;

        auto NewExplosion = [&](fvec3 pos, float size)
        {
            Explosion tmp;
            tmp.pos = pos;
            tmp.target_size = size;
            tmp.state = 0;
            tmp.decr = 0;
            explosions.push_back(tmp);
        };



        int net_kick_timer = 0;
        bool net_got_enemy_shots = 0;



        auto World_Tick = [&]
        {
            // Selecting a cell
            if (Input::MouseButtonPressed(1) && state == State::selecting_cells && Info::WorldBoardCameraPos().x != -1 && !menu_open)
            {
                first_turn_hint = 0;

                if (enemy_hit_map[Info::WorldBoardCameraPos().y][Info::WorldBoardCameraPos().x] == -1)
                {
                    bool overlap = 0;
                    for (auto it = shots_list.begin(); it != shots_list.end(); it++)
                    {
                        if (*it == Info::WorldBoardCameraPos())
                        {
                            overlap = 1;
                            remaining_shots++;
                            shots_list.erase(it);
                            Sounds::remove_target->Play(1, Info::rng() % 1000 / 1000.0f + 0.5f);
                            break;
                        }
                    }

                    if (!overlap)
                    {
                        if (remaining_shots)
                        {
                            Sounds::ship_rotation->Play(1, Info::rng() % 1000 / 1000.0f + 0.5f);
                            shots_list.push_back(Info::WorldBoardCameraPos());
                            remaining_shots--;
                        }
                        else
                            Sounds::no_shots->Play(1, Info::rng() % 1000 / 1000.0f + 0.5f);
                    }
                }
                else
                    Sounds::no_shots->Play(1, Info::rng() % 1000 / 1000.0f + 0.5f);
            }


            // Bullets
            if (state != State::selecting_cells && state != State::waiting_for_data)
            {
                bool play_pew_sound = 0;

                auto BulletHandling = [&](int &t_to_next, std::vector<ivec2> &sh_list, int *own_hp, int &what_sh_next, Board &own_board, int pos_mult, std::vector<Bullet> &blt_list, int &sh_counter, Board &opp_board, int *opp_hp, int (*hitmap)[20])
                {
                    // Shooting
                    t_to_next--;
                    if (t_to_next < 0)
                    {
                        t_to_next = delay_between_bullets;

                        if (sh_list.size() != 0)
                        {
                            play_pew_sound = 1;

                            ivec2 target = *sh_list.begin();
                            sh_list.erase(sh_list.begin());


                            // Setting up bullet
                            while (own_hp[what_sh_next] == 0)
                                what_sh_next++;

                            fvec2 source_ship_pos = own_board.ship_positions[what_sh_next];

                            Bullet tmp;
                            tmp.target = target;
                            if (pos_mult == 1) // Player shoots
                                tmp.src = {(-boards_displacement+0.5f)*pos_mult, source_ship_pos.x*pos_mult, -source_ship_pos.y};
                            else // Enemy shoots - we need to hide the source
                                tmp.src = {(-boards_displacement+0.5f)*pos_mult, float(int(Info::rng() % 10001) - 5000)/500, float(int(Info::rng() % 10001) - 5000)/500};
                            tmp.dst = {boards_displacement*pos_mult, -(target.x-9.5f)*pos_mult, -(target.y-9.5f)};
                            tmp.pos = {0,0,0};
                            tmp.dist = 0;
                            tmp.rotation = Quaternion::from_axis_and_angle(fvec3{float(pos_mult),0,0}.cross(tmp.dst - tmp.src), std::asin(fvec3{1,0,0}.cross(tmp.dst - tmp.src).len() / (tmp.dst - tmp.src).len())).normalize_and_get_matrix();
                            tmp.hit = 0;
                            blt_list.push_back(tmp);

                            sh_counter++;
                            if (sh_counter >= 4)
                            {
                                sh_counter = 0;
                                what_sh_next++;
                            }
                        }
                    }



                    // Tick
                    auto it = blt_list.begin();
                    while (it != blt_list.end())
                    {
                        it->pos = (it->dst - it->src) * it->dist + it->src;

                        it->dist += bullet_distance_per_tick;

                        // Collision
                        if (it->hit == 0 && it->dist > 1)
                        {
                            it->hit = 1;

                            if (opp_board.At(it->target) > 1)
                            { // Hit
                                int ship_size = opp_board.At(it->target);
                                int ship_id = 6 - ship_size;
                                hitmap[it->target.y][it->target.x] = ship_id;
                                opp_hp[ship_id]--;

                                if (opp_hp[ship_id] == 0)
                                {
                                    Sounds::ship_dead->Play(1, Info::rng() % 1000 / 1000.0f + 0.5f);

                                    fvec2 ship_pos = opp_board.ship_positions[ship_id];

                                    NewExplosion({boards_displacement*pos_mult, -ship_pos.x*pos_mult, -ship_pos.y}, ship_size);

                                    // Place dots around destroyed ships
                                    for (int y = 0; y < 20; y++)
                                    {
                                        for (int x = 0; x < 20; x++)
                                        {
                                            if (hitmap[y][x] != -1)
                                                continue;

                                            for (const ivec2 &offset : Config::full_circle_offsets)
                                            {
                                                if (opp_board.At(ivec2{x,y} + offset) <= 1)
                                                    continue;
                                                if (opp_hp[6-opp_board.At(ivec2{x,y} + offset)] == 0)
                                                {
                                                    hitmap[y][x] = -2;
                                                    break;
                                                }
                                            }
                                        }
                                    }


                                    // If this was the last ship
                                    if (state != State::finished)
                                    {
                                        bool any_ships_left = 0;
                                        for (int i = 0; i < 5; i++)
                                        {
                                            if (opp_hp[i] != 0)
                                            {
                                                any_ships_left = 1;
                                                break;
                                            }
                                        }
                                        if (!any_ships_left)
                                        {
                                            state = State::finished;
                                            player_won = (&own_board == &player_board);
                                        }
                                    }
                                }
                                else
                                {
                                    Sounds::ship_hit->Play(1, Info::rng() % 1000 / 1000.0f + 0.5f);
                                }

                                NewExplosion(it->dst, 1);
                            }
                            else
                            { // Miss
                                hitmap[it->target.y][it->target.x] = -2;
                            }
                        }

                        // Distance cap
                        if (it->dist > 8)
                        {
                            it = blt_list.erase(it);

                            continue;
                        }

                        it++;
                    }
                };


                BulletHandling(time_to_next_bullet, shots_list, player_ship_hp, what_ship_shoots_next, player_board, 1, bullets, shot_ship_counter, enemy_board, enemy_ship_hp, enemy_hit_map);

                BulletHandling(other_time_to_next_bullet, other_shots_list, enemy_ship_hp, other_what_ship_shoots_next, enemy_board, -1, other_bullets, other_shot_ship_counter, player_board, player_ship_hp, other_enemy_hit_map);


                // Playing pew sound
                if (play_pew_sound)
                    Sounds::ship_shot->Play(1, Info::rng() % 1000 / 1000.0f + 0.5f);


                // Changing state when no more bullets
                if (state != State::finished && bullets.size() == 0 && other_bullets.size() == 0 && shots_list.size() == 0 && other_shots_list.size() == 0)
                {
                    remaining_shots = 0;
                    for (int it : player_ship_hp)
                        if (it > 0)
                            remaining_shots += 4;

                    enemy_remaining_shots = 0;
                    for (int it : enemy_ship_hp)
                        if (it > 0)
                            enemy_remaining_shots += 4;

                    state = State::selecting_cells;
                }
            }



            // Fire effect for damaged ships
            auto FireEffect = [&](Board &board, int (*hitmap)[20], int *hp, int multiplicator)
            {
                for (int y = 0; y < 20; y++)
                {
                    for (int x = 0; x < 20; x++)
                    {
                        int ship = board.At({x,y});
                        if (ship > 1)
                        {
                            ship = 6 - ship;
                            if (hitmap[y][x] != -1 && hp[ship] != 0)
                            {
                                if (Info::rng() % fire_prob_divisor != 0)
                                    continue;

                                auto RandomOffset = [&]() -> float
                                {
                                    return (int(Info::rng() % 10001) - 5000) / 5000.0f;
                                };

                                fvec3 center{-boards_displacement*multiplicator, (x-9.5f)*multiplicator, -(y-9.5f)};
                                NewExplosion(center + fvec3{RandomOffset()/fire_size_divisor,RandomOffset()/fire_size_divisor,RandomOffset()/fire_size_divisor}, RandomOffset()/4+0.3f);
                            }
                        }
                    }
                }
            };
            FireEffect(enemy_board, enemy_hit_map, enemy_ship_hp, -1);
            FireEffect(player_board, other_enemy_hit_map, player_ship_hp, 1);

            // Camera
            if (state != State::selecting_cells)
            {
                camera_distance += 0.03;
                if (camera_distance > 1)
                    camera_distance = 1;
            }
            else
            {
                camera_distance -= 0.03;
                if (camera_distance < 0)
                    camera_distance = 0;
            }

            Shaders::Cameras::Observer(camera_distance, !menu_open);
        };

        auto World_Render = [&]
        {
            // Explosions
            if (explosions.size() != 0)
            {
                fmat4 saved_matrix = Shaders::world_model_mat;

                auto it = explosions.begin();
                while (it != explosions.end())
                {
                    if (!it->decr)
                    {
                        it->state += expl_grow_speed;

                        if (it->state > 1)
                        {
                            it->state = 1;
                            it->decr = 1;
                        }
                    }
                    else
                    {
                        it->state -= expl_destr_speed;

                        if (it->state < 0)
                        {
                            it = explosions.erase(it);
                            continue;
                        }
                    }

                    float true_size = (2*it->state*it->state*it->state - 3*it->state*it->state) * it->target_size / 10;

                    Shaders::world_model_mat = saved_matrix /mul/ fmat4::translate(it->pos) /mul/ fmat4::dia({true_size,true_size,true_size});
                    //Shaders::world_model_mat = fmat4::dia({true_size,true_size,true_size}).mul(fmat4::translate(it->pos)).mul(saved_matrix);
                    Shaders::SendMatricesToWorldShader();
                    if (!it->decr)
                        Shaders::WorldColorFactor({it->state*4, it->state, 0});
                    else
                        Shaders::WorldColorFactor({1, 1, 1 - it->state});

                    Models::explosion->DrawTriangles();

                    it++;
                }

                Shaders::world_model_mat = saved_matrix;
                Shaders::SendMatricesToWorldShader();
                Shaders::WorldColorFactor();
            }


            // Selection ray on enemy board
            if (state == State::selecting_cells)
            {
                fvec2 selection_ray = Info::WorldBoardCameraPosFloat();
                if (selection_ray.x > -0.5f)
                {
                    Shaders::WorldColorFactor({0.75,0.75,0.75});

                    fmat4 saved_matrix = Shaders::world_model_mat;

                    Shaders::world_model_mat = saved_matrix /mul/ fmat4::translate({boards_displacement, -(selection_ray.x-10), -(selection_ray.y-10)}) /mul/ fmat4::dia({0.02,0.02,0.05}) /mul/ fmat4::rotate_with_normalized_axis({0,1,0},to_rad(90.0));
                    //Shaders::world_model_mat = fmat4::rotate_with_normalized_axis({0,1,0},to_rad(90.0)).mul(fmat4::dia({0.02,0.02,0.05})).mul(fmat4::translate({boards_displacement, -(selection_ray.x-10), -(selection_ray.y-10)})).mul(saved_matrix);
                    Shaders::SendMatricesToWorldShader();
                    Models::bullet->DrawTriangles();

                    Shaders::world_model_mat = saved_matrix /mul/ fmat4::translate({boards_displacement, -(selection_ray.x-10), -(selection_ray.y-10)}) /mul/ fmat4::dia({0.1,0.02,0.02});
                    //Shaders::world_model_mat = fmat4::dia({0.1,0.02,0.02}).mul(fmat4::translate({boards_displacement, -(selection_ray.x-10), -(selection_ray.y-10)})).mul(saved_matrix);
                    Shaders::SendMatricesToWorldShader();
                    Models::bullet->DrawTriangles();

                    Shaders::world_model_mat = saved_matrix /mul/ fmat4::translate({boards_displacement, -(selection_ray.x-10), -(selection_ray.y-10)}) /mul/ fmat4::dia({0.02,0.05,0.02}) /mul/ fmat4::rotate_with_normalized_axis({0,0,1},to_rad(90.0));
                    //Shaders::world_model_mat = fmat4::rotate_with_normalized_axis({0,0,1},to_rad(90.0)).mul(fmat4::dia({0.02,0.05,0.02})).mul(fmat4::translate({boards_displacement, -(selection_ray.x-10), -(selection_ray.y-10)})).mul(saved_matrix);
                    Shaders::SendMatricesToWorldShader();
                    Models::bullet->DrawTriangles();

                    Shaders::world_model_mat = saved_matrix;
                    Shaders::SendMatricesToWorldShader();

                    Shaders::WorldColorFactor();
                }
            }


            // Bullets
            if (state != State::selecting_cells)
            {
                fmat4 saved_matrix = Shaders::world_model_mat;

                for (const auto &it : bullets)
                {
                    Shaders::world_model_mat = fmat4::translate(it.pos) /mul/ it.rotation.to_mat4x4() /mul/ fmat4::dia({it.dist*2,1.f/12,1.f/12});
                    //Shaders::world_model_mat = fmat4::dia({it.dist*2,1.f/12,1.f/12}).mul(it.rotation.to_mat4x4()).mul(fmat4::translate(it.pos));
                    Shaders::SendMatricesToWorldShader();

                    Shaders::WorldColorFactor({std::min(it.dist*5,1.0f),std::min(it.dist,0.6f),0});

                    Models::bullet->DrawTriangles();
                }

                for (const auto &it : other_bullets)
                {
                    Shaders::world_model_mat = fmat4::translate(it.pos) /mul/ it.rotation.to_mat4x4() /mul/ fmat4::dia({it.dist*2,1.f/12,1.f/12});
                    //Shaders::world_model_mat = fmat4::dia({it.dist*2,1.f/12,1.f/12}).mul(it.rotation.to_mat4x4()).mul(fmat4::translate(it.pos));
                    Shaders::SendMatricesToWorldShader();

                    Shaders::WorldColorFactor({std::min(it.dist/10,1.0f),std::min(it.dist,1.0f),std::min(it.dist*5,1.0f)});

                    Models::bullet->DrawTriangles();
                }

                Shaders::WorldColorFactor();

                Shaders::world_model_mat = saved_matrix;
                Shaders::SendMatricesToWorldShader();
            }


            { // Boards
                auto DrawEnemyBoard = [&]
                {
                    Shaders::world_model_mat = fmat4::translate({boards_displacement,0,0}) /mul/ fmat4::rotate_with_normalized_axis({0,1,0}, to_rad(-90.0)) /mul/ fmat4::rotate_with_normalized_axis({0,0,-1},to_rad(90.0));
                    //Shaders::world_model_mat = fmat4::rotate_with_normalized_axis({0,0,-1},to_rad(90.0)).mul(fmat4::rotate_with_normalized_axis({0,1,0}, to_rad(-90.0))).mul(fmat4::translate({boards_displacement,0,0}));
                    Shaders::SendMatricesToWorldShader();

                    bool render_hidden_ships = game_over_timer >= game_over_msg_full_alpha_time;
                    float hidden_ships_alpha = std::min((game_over_timer - game_over_msg_full_alpha_time) / float(game_over_timer_cap - game_over_msg_full_alpha_time), 1.0f);

                    if (camera_distance == 0 && Info::WorldBoardCameraPos().x != -1)
                        enemy_board.Render3D(0.3, Info::WorldBoardCameraPos(), (state == State::selecting_cells ? shots_list : std::vector<ivec2>{}), &enemy_hit_map, &enemy_ship_hp, !render_hidden_ships, state == State::selecting_cells, hidden_ships_alpha);
                    else
                        enemy_board.Render3D(0.3, {-1,-1},                     (state == State::selecting_cells ? shots_list : std::vector<ivec2>{}), &enemy_hit_map, &enemy_ship_hp, !render_hidden_ships, state == State::selecting_cells, hidden_ships_alpha);
                };
                auto DrawPlayerBoard = [&]
                {
                    Shaders::world_model_mat = fmat4::translate({-boards_displacement,0,0}) /mul/ fmat4::rotate_with_normalized_axis({0,1,0}, to_rad(90.0)) /mul/ fmat4::rotate_with_normalized_axis({0,0,-1},to_rad(-90.0));
                    //Shaders::world_model_mat = fmat4::rotate_with_normalized_axis({0,0,-1},to_rad(-90.0)).mul(fmat4::rotate_with_normalized_axis({0,1,0}, to_rad(90.0))).mul(fmat4::translate({-boards_displacement,0,0}));
                    Shaders::SendMatricesToWorldShader();
                    player_board.Render3D(0.2, {-1,-1}, {}, &other_enemy_hit_map, &player_ship_hp, 0, camera_distance == 0);
                };

                if (Shaders::world_cam_xa > -to_rad(90.0) && Shaders::world_cam_xa < to_rad(90.0))
                {
                    DrawEnemyBoard();
                    DrawPlayerBoard();
                }
                else
                {
                    DrawPlayerBoard();
                    DrawEnemyBoard();
                }
            }
        };

        auto GUI_Tick = [&]
        {
            if (exiting)
            {
                fade--;
                return;
            }
            else if (fade != Config::fade_upper_cap)
            {
                fade++;
            }


            // Finishing the turn
            if (state == State::selecting_cells && !menu_open)
            {
                if (Input::MouseButtonDown(3))
                {
                    if (remaining_shots != 0)
                        turn_end_timer++;
                    else
                        turn_end_timer += 4;

                    auto EndTurnSeq = [&]
                    {
                        turn_end_timer = 0;
                        Sounds::end_turn->Play();

                        time_to_next_bullet = delay_before_first_bullet;
                        bullets.clear();
                        what_ship_shoots_next = 0;
                        shot_ship_counter = 0;


                        other_time_to_next_bullet = delay_before_first_bullet;
                        if (Net::netplay == 0)
                            other_bullets.clear();
                        other_what_ship_shoots_next = 0;
                        other_shot_ship_counter = 0;


                        first_turn_hint = 0;
                    };

                    if (turn_end_timer > turn_end_time_cap)
                    {
                        if (Net::netplay == 0)
                        {
                            // Generating enemy shots
                            std::vector<ivec2> possible_spots;

                            // Make a list of all possible shots
                            for (int y = 0; y < 20; y++)
                            {
                                for (int x = 0; x < 20; x++)
                                {
                                    if (other_enemy_hit_map[y][x] == -1)
                                        possible_spots.push_back({x,y});
                                }
                            }

                            // Debug feature: bot always hits perfectly
                            if (bot_always_hits_perfectly)
                            {
                                auto it = possible_spots.begin();
                                while (it != possible_spots.end() && enemy_remaining_shots > 0)
                                {
                                    if (player_board.At(*it) > 1)
                                    {
                                        other_shots_list.push_back(*it);
                                        it = possible_spots.erase(it);
                                        enemy_remaining_shots--;
                                    }
                                    else
                                        it++;
                                }
                            }

                            // Shooting discovered ships
                            for (const ivec2 &offset : Config::extended_circle_offsets)
                            {
                                if (enemy_remaining_shots <= 0)
                                    break;
                                auto it = possible_spots.begin();
                                while (it != possible_spots.end() && enemy_remaining_shots > 0)
                                {
                                    if ((*it+offset >= 0) && (*it+offset < 20) && other_enemy_hit_map[it->y+offset.y][it->x+offset.x] >= 0)
                                    {
                                        other_shots_list.push_back(*it);
                                        it = possible_spots.erase(it);
                                        enemy_remaining_shots--;
                                        continue;
                                    }

                                    it++;
                                }
                            }

                            // Making random shots
                            while (enemy_remaining_shots > 0 && possible_spots.size() > 0)
                            {
                                int pos = Info::rng() % possible_spots.size();

                                other_shots_list.push_back(possible_spots[pos]);

                                possible_spots.erase(possible_spots.begin() + pos);

                                enemy_remaining_shots--;
                            }

                            // Make sure that a least one player made a shot
                            if (shots_list.size() != 0 || other_shots_list.size() != 0) // If so, change the state
                            {
                                state = State::watching_battle;
                                EndTurnSeq();
                            }
                            else // If no one made a shot, then we do not change state
                            {
                                turn_end_timer = 0;
                            }
                        }
                        else
                        {
                            state = State::waiting_for_data;

                            if (Net::SendShots(shots_list) == 0)
                            {
                                Sys::SetCurrentFunction(Menu);
                                exiting = 1;
                            }

                            EndTurnSeq();
                        }
                    }
                }
                else
                {
                    turn_end_timer = 0;
                }
            }
            else
                turn_end_timer = 0;


            // Menu
            menu_show = menu_open;
            if (Input::KeyPressed(Input::Key_Esc()) && (state != State::finished || menu_open == 1))
            {
                Sounds::open_menu->Play();

                menu_open = !menu_open;
                Input::RelativeMouseMode(!menu_open);
                if (menu_open)
                    Input::SetMousePos(ivec2{0,int(Window::Size().y)/6} + ((ivec2)Window::Size() + 1) / 2);
                else
                    menu_show = 0;
            }
            if (menu_show)
            {
                for (int i = 0; i < 2; i++)
                {
                    if (i == menu_selected_button)
                    {
                        if (menu_buttons_alpha[i] != Config::menu_button_timer_upper_cap)
                            menu_buttons_alpha[i]++;
                    }
                    else
                    {
                        if (menu_buttons_alpha[i] != 0)
                            menu_buttons_alpha[i]--;
                    }
                }

                if (Info::GuiMouseInRect({-menu_button_collider_w, menu_y_offset-3-25}, {menu_button_collider_w*2,24}))
                    menu_selected_button = 0;
                else if (Info::GuiMouseInRect({-menu_button_collider_w, menu_y_offset-3}, {menu_button_collider_w*2,24}))
                    menu_selected_button = 1;
                else
                    menu_selected_button = -1;

                // Click
                if (Input::MouseButtonPressed(1))
                {
                    switch (menu_selected_button)
                    {
                      case 0:
                        Sounds::menu_click->Play();
                        menu_open = 0;
                        break;
                      case 1:
                        Sounds::menu_click->Play();
                        Sys::SetCurrentFunction(Menu);
                        exiting = 1;
                    }
                }
            }
            else
            {
                menu_selected_button = -1;
                menu_buttons_alpha[0] = 0;
                menu_buttons_alpha[1] = 0;
            }

            // Game over stuff
            if (state == State::finished && bullets.size() == 0 && other_bullets.size() == 0)
            {
                if (game_over_timer != game_over_timer_cap)
                {
                    game_over_timer++;
                }
                else
                {
                    if ((Input::AnyMouseButtonPressed() || Input::AnyKeyPressed()) && !menu_open)
                    {
                        Sounds::menu_click->Play();
                        Sys::SetCurrentFunction(Menu);
                        exiting = 1;
                    }
                }
            }


            // Network
            if (Net::netplay)
            {
                // Send ping
                if (Net::SendPing() == 0 && state != State::finished)
                {
                    Sys::SetCurrentFunction(Menu);
                    exiting = 1;
                }

                // Receive
                if (Net::ReadData(net_kick_timer, 0, 0, &other_shots_list, &net_got_enemy_shots) == 0 && state != State::finished)
                {
                    Sys::SetCurrentFunction(Menu);
                    exiting = 1;
                }

                // Waiting for shots
                if (state == State::waiting_for_data && net_got_enemy_shots)
                {
                    net_got_enemy_shots = 0;
                    state = State::watching_battle;
                }
            }
        };

        auto GUI_Render = [&]
        {
            if (state == State::selecting_cells)
            {
                // Remaining shots
                Draw::GUI::TextL(Locale::rem_shots(), -Info::gui_texture_size/2 + 4);
                Draw::GUI::NumberLargeL(remaining_shots, -Info::gui_texture_size/2 + ivec2{3,16});

                // End turn hint and bar
                if (!menu_open)
                {
                    if (!Input::MouseButtonDown(3))
                    {
                        Draw::GUI::TextC(Locale::end_turn_hint(), {0, Info::gui_texture_size.y/2 - 16});
                    }
                    else
                    {
                        if (remaining_shots != 0)
                            Draw::GUI::TextC(Locale::spend_your_shots(), {0,32}, {1,1,1,1});

                        float normed_timer = (turn_end_timer / float(turn_end_time_cap));

                        Draw::GUI::RectFill({(int)std::round(-turn_end_bar_end_x * normed_timer), -8}, {(int)std::round(turn_end_bar_end_x*2 * normed_timer), 16}, {1,0.5,0,normed_timer});

                        Draw::GUI::Rect({-turn_end_bar_end_x-8,-8}, {8,16}, {144,80}, {8,16}, {1,0.5,0,0}, {1,1-normed_timer});
                        Draw::GUI::Rect({turn_end_bar_end_x,-8}, {8,16}, {152,80}, {8,16}, {1,0.5,0,0}, {1,1-normed_timer});

                    }
                }
            }

            // Game over messages
            if (state == State::finished && game_over_timer > 0)
            {
                float alpha = std::min(game_over_timer / float(game_over_msg_full_alpha_time), 1.0f);
                Draw::GUI::Rect({-game_over_msg_background_end_x, -Info::gui_texture_size.y/2+game_over_msg_y_displacement}, {game_over_msg_background_end_x*2, 20}, {96,168}, {112,24}, {0,0,0,0}, {0,1-alpha});
                Draw::GUI::TextLargeC(player_won ? Locale::victory() : Locale::defeat(), ivec2{0,-Info::gui_texture_size.y/2+game_over_msg_y_displacement+game_over_msg_text_offset}+1, {0,0,0,alpha});
                Draw::GUI::TextLargeC(player_won ? Locale::victory() : Locale::defeat(), {0,-Info::gui_texture_size.y/2+game_over_msg_y_displacement+game_over_msg_text_offset}, player_won ? fvec4{1,0.8,0,alpha} : fvec4{0.75,0,0,alpha});

                if (game_over_timer > game_over_msg_full_alpha_time)
                {
                    float alpha = std::min((game_over_timer - game_over_msg_full_alpha_time) / float(game_over_timer_cap - game_over_msg_full_alpha_time), 1.0f);
                    Draw::GUI::TextC(Locale::any_key(), {0, Info::gui_texture_size.y/2 - game_over_press_any_key_displacement}, {1,1,1,alpha});
                }
            }

            // First turn hint
            if (first_turn_hint && turn_end_timer == 0 && !menu_open)
            {
                Draw::GUI::TextC(Locale::first_turn_hint(), {0, first_turn_hint_offset});
            }

            // Network waiting for opponent hint
            if (state == State::waiting_for_data && camera_distance == 1)
            {
                Draw::GUI::TextC(Locale::waiting_for_enemy_shots(), {0, Info::gui_texture_size.y/2-16});
            }

            // Menu
            if (menu_show)
            {
                Draw::GUI::RectFill(-menu_rect_sz/2-menu_rect_margin, menu_rect_sz+menu_rect_margin*2, {0,0,0,0.6});
                Draw::GUI::RectFill(-menu_rect_sz/2, menu_rect_sz, {0,0,0,1});

                bool button0_offset = (menu_selected_button == 0) && Input::MouseButtonPressed(1);
                bool button1_offset = (menu_selected_button == 1) && Input::MouseButtonPressed(1);
                float button0_alpha = menu_buttons_alpha[0] / float(Config::menu_button_timer_upper_cap);
                float button1_alpha = menu_buttons_alpha[1] / float(Config::menu_button_timer_upper_cap);

                Draw::GUI::TextLargeC(Locale::game_menu_continue(), ivec2{0,-25+menu_y_offset+button0_offset}+1, Config::menu_text_shadow_color.interpolate(Config::menu_text_selected_shadow_color, button0_alpha)); // Fixed
                Draw::GUI::TextLargeC(Locale::game_menu_continue(), {0,-25+menu_y_offset+button0_offset}, Config::menu_text_color.interpolate(Config::menu_text_selected_color, button0_alpha)); // Fixed
                Draw::GUI::TextLargeC(Locale::game_menu_quit(), ivec2{0,menu_y_offset+button1_offset}+1, Config::menu_text_shadow_color.interpolate(Config::menu_text_selected_shadow_color, button1_alpha)); // Fixed
                Draw::GUI::TextLargeC(Locale::game_menu_quit(), {0,menu_y_offset+button1_offset}, Config::menu_text_color.interpolate(Config::menu_text_selected_color, button1_alpha)); // Fixed

                Draw::GUI::Cursor();
            }
        };


        Graphics::Depth(1);

        Utils::TickStabilizer ts(60,8);

        while (1)
        {
            Sys::BeginFrame();

            while (ts.Tick())
            {
                Sys::Tick();
                World_Tick();
                GUI_Tick();
            }

            Shaders::world->Use();


            Graphics::Depth(0);
            Queues::world->Clear();
            Shaders::WorldComputeViewMatrix({0,0,0});
            Shaders::world_model_mat = fmat4::identity();
            Shaders::SendMatricesToWorldShader();
            Draw::World::Skybox();
            Queues::world->Send();
            Queues::world->DrawTriangles();


            Graphics::Depth(1);
            Shaders::WorldComputeViewMatrix();
            Shaders::SendMatricesToWorldShader();
            World_Render();


            Graphics::Depth(0);


            Shaders::flat->Use();
            Framebuffers::gui->BindDrawAndClear();
            Graphics::ViewportObj(*Framebuffers::gui_texture);
            Queues::gui->Clear();

            GUI_Render();

            Queues::gui->Send();
            Queues::gui->DrawTriangles();

            Shaders::gui->Use();
            Shaders::SendMousePosToGuiShader(menu_show);

            Graphics::Framebuffer::UnbindDraw();
            Graphics::ViewportFullscreen2y();
            Models::GUI::quad->DrawTriangles(0,6);

            Shaders::flat->Use();
            if (fade != Config::fade_upper_cap)
            {
                Queues::gui->Clear();
                Draw::GUI::RectFill(-Info::gui_texture_size/2, Info::gui_texture_size, {0,0,0,1 - float(fade)/Config::fade_upper_cap});
                Queues::gui->Send();
                Queues::gui->DrawTriangles();
            }

            if (exiting && fade <= 0)
            {
                Net::Close();
                return;
            }

            Graphics::Depth(1);

            Sys::EndFrame();
        }
    }
}
