# What is this project? #
This is a small game I've created for fun some time ago.
Basically, it is a slightly reworked classical Battleship game, with bigger board and more ships, singleplayer and multiplayer.

# Localizations #
The game is translated into English and Russian.

# Author #
Just me:
HolyBlackCat (Egor Mikhailov)

(Except one skybox that was found on the internet.)

You can contact me in Skype (login:iamsupermouse).

# Screenshots #
![1.png](https://bitbucket.org/repo/kr5Lqp/images/2509272542-1.png) ![2.png](https://bitbucket.org/repo/kr5Lqp/images/1877463003-2.png)

![3.png](https://bitbucket.org/repo/kr5Lqp/images/892726892-3.png) ![4.png](https://bitbucket.org/repo/kr5Lqp/images/2281876839-4.png)